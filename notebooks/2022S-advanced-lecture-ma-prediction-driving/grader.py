# !pip install torch
import IPython
from nose.tools import assert_equal, ok_
from IPython.display import display_html
import numpy as np
import torch


def test_ok():
    """ If execution gets to this point, print out a happy message """
    try:
        from IPython.display import display_html
        display_html("""<div class="alert alert-success">
        <strong>Tests passed!!</strong>
        </div>""", raw=True)
    except:
        print("Tests passed!!")


"""
Sanity check
"""
def check_sanity(x):
    assert_equal(x, 10)

"""
Check dynamics model
"""
def check_agent_modeling(agent):
    agent.update_state(0.1)
    assert_equal(agent.state.x, 1.1)
    assert_equal(agent.state.y, 2.1)
    assert_equal(agent.state.x_dot, 1)
    assert_equal(agent.state.y_dot, 1)
    assert_equal(agent.state.t, 0.1)

"""
Check state array
"""
def check_agent_state_array(state_array):
    x_sub = []
    y_sub = []
    for i in state_array:
        x_sub.append(i.x)
        y_sub.append(i.y)
        
    x_ans = [5, 4.94, 4.880000000000001, 4.820000000000001, 4.760000000000002, 4.700000000000002, 4.640000000000002, 4.580000000000003, 4.520000000000003, 4.4600000000000035, 4.400000000000004, 4.340000000000004, 4.280000000000005, 4.220000000000005, 4.1600000000000055, 4.100000000000006, 4.040000000000006, 3.980000000000006, 3.920000000000006, 3.860000000000006, 3.800000000000006, 3.740000000000006, 3.680000000000006, 3.620000000000006, 3.560000000000006, 3.5000000000000058, 3.4400000000000057, 3.3800000000000057, 3.3200000000000056, 3.2600000000000056, 3.2000000000000055, 3.1400000000000055, 3.0800000000000054, 3.0200000000000053, 2.9600000000000053, 2.9000000000000052, 2.840000000000005, 2.780000000000005, 2.720000000000005, 2.660000000000005, 2.600000000000005, 2.540000000000005, 2.480000000000005, 2.420000000000005, 2.3600000000000048, 2.3000000000000047, 2.2400000000000047, 2.1800000000000046, 2.1200000000000045, 2.0600000000000045, 2.0000000000000044, 1.9400000000000044, 1.8800000000000043, 1.8200000000000043, 1.7600000000000042, 1.7000000000000042, 1.6400000000000041, 1.580000000000004, 1.520000000000004, 1.460000000000004, 1.400000000000004, 1.3400000000000039, 1.2800000000000038, 1.2200000000000037, 1.1600000000000037, 1.1000000000000036, 1.0400000000000036, 0.9800000000000035, 0.9200000000000035, 0.8600000000000034, 0.8000000000000034, 0.7400000000000033, 0.6800000000000033, 0.6200000000000032, 0.5600000000000032, 0.5000000000000031, 0.4400000000000031, 0.3800000000000031, 0.3200000000000031, 0.2600000000000031, 0.20000000000000312, 0.14000000000000312, 0.08000000000000312, 0.020000000000003126, -0.03999999999999687, -0.09999999999999687, -0.15999999999999687, -0.21999999999999686, -0.27999999999999686, -0.33999999999999686, -0.39999999999999686, -0.45999999999999686, -0.5199999999999969, -0.579999999999997, -0.639999999999997, -0.6999999999999971, -0.7599999999999971, -0.8199999999999972, -0.8799999999999972, -0.9399999999999973, -0.9999999999999973]
    y_ans = [5, 4.94, 4.880000000000001, 4.820000000000001, 4.760000000000002, 4.700000000000002, 4.640000000000002, 4.580000000000003, 4.520000000000003, 4.4600000000000035, 4.400000000000004, 4.340000000000004, 4.280000000000005, 4.220000000000005, 4.1600000000000055, 4.100000000000006, 4.040000000000006, 3.980000000000006, 3.920000000000006, 3.860000000000006, 3.800000000000006, 3.740000000000006, 3.680000000000006, 3.620000000000006, 3.560000000000006, 3.5000000000000058, 3.4400000000000057, 3.3800000000000057, 3.3200000000000056, 3.2600000000000056, 3.2000000000000055, 3.1400000000000055, 3.0800000000000054, 3.0200000000000053, 2.9600000000000053, 2.9000000000000052, 2.840000000000005, 2.780000000000005, 2.720000000000005, 2.660000000000005, 2.600000000000005, 2.540000000000005, 2.480000000000005, 2.420000000000005, 2.3600000000000048, 2.3000000000000047, 2.2400000000000047, 2.1800000000000046, 2.1200000000000045, 2.0600000000000045, 2.0000000000000044, 1.9400000000000044, 1.8800000000000043, 1.8200000000000043, 1.7600000000000042, 1.7000000000000042, 1.6400000000000041, 1.580000000000004, 1.520000000000004, 1.460000000000004, 1.400000000000004, 1.3400000000000039, 1.2800000000000038, 1.2200000000000037, 1.1600000000000037, 1.1000000000000036, 1.0400000000000036, 0.9800000000000035, 0.9200000000000035, 0.8600000000000034, 0.8000000000000034, 0.7400000000000033, 0.6800000000000033, 0.6200000000000032, 0.5600000000000032, 0.5000000000000031, 0.4400000000000031, 0.3800000000000031, 0.3200000000000031, 0.2600000000000031, 0.20000000000000312, 0.14000000000000312, 0.08000000000000312, 0.020000000000003126, -0.03999999999999687, -0.09999999999999687, -0.15999999999999687, -0.21999999999999686, -0.27999999999999686, -0.33999999999999686, -0.39999999999999686, -0.45999999999999686, -0.5199999999999969, -0.579999999999997, -0.639999999999997, -0.6999999999999971, -0.7599999999999971, -0.8199999999999972, -0.8799999999999972, -0.9399999999999973, -0.9999999999999973]
    assert_equal(x_ans, x_sub)
    assert_equal(y_ans, y_sub)

"""
Check classification
"""
def check_interaction_label_1(label):
    assert_equal("GOING", label)

def check_interaction_label_2(label):
    assert_equal("YIELDING", label)

def check_interaction_label_3(label):
    assert_equal("IGNORING", label)

"""
Check Graph Net Creation
"""
def check_create_intersection_graph_net(graph):
    agents = set(["B", "L","G","P"])
    elements = set(["S1","S2"])
    agent_element_pairs = [["B","S1"], ["P","S1"],["P","S2"],["G","S2"]]

    for n in agents.union(elements):
        if n not in graph.nodes():
           raise AssertionError("Graph is missing the following node: {}".format(n))
        
    for n1 in agents:
        for n2 in agents:
            if n1!=n2:
                if not graph.has_edge(n1,n2):
                   raise AssertionError("Graph is missing the following edge ({},{})".format(n1,n2))
    
    for pair in agent_element_pairs:
        a,b = pair
        if not graph.has_edge(a,b):
           raise AssertionError("Graph is missing the following edge ({},{})".format(a,b))
        if not graph.has_edge(b,a):
           raise AssertionError("Graph is missing the following edge ({},{})".format(b,a))
                
                
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)



# +
def test_ok_green_car_prior_state(mat):
    np.testing.assert_array_equal(mat, np.array([[50,37,2],[53,37,7],[58,37,10],[60,37,12]]))
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)
    
def test_ok_police_car_prior_state(mat):
    np.testing.assert_array_equal(mat, np.array([[38,30,16],[38,22,16],[38,14,16],[38,6,16]]))
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)

def test_ok_lime_car_prior_state(mat):
    np.testing.assert_array_equal(mat, np.array([[18,32,4],[16,32,4],[13,32,6],[9,32,8]]))
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)
    
def test_ok_blue_car_prior_state(mat):
    np.testing.assert_array_equal(mat, np.array([[9,32,2],[8,32,2],[6,32,4],[3,32,6]]))
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)
    
def test_ok_stop_sign_1_prior_state(mat):
    np.testing.assert_array_equal(mat, np.array([[28,28,0],[28,28,0],[28,28,0],[28,28,0]]))
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)
    
def test_ok_stop_sign_2_prior_state(mat):
    np.testing.assert_array_equal(mat, np.array([[42,42,0],[42,42,0],[42,42,0],[42,42,0]]))
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)


# +
def test_ok_one_hot_encode_node_type_vehicle(encoding):
    np.testing.assert_array_equal(np.array([1,0,0,0,0,0,0]), encoding)
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)
    
def test_ok_one_hot_encode_node_type_stop_sign(encoding):
    np.testing.assert_array_equal(np.array([0,1,0,0,0,0,0]), encoding)
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)


# -

def check_intersection_graph_net_edge_list(edge_list):
    user_edge_set = set([(edge_list[0][i],edge_list[1][i]) for i in range(len(edge_list[0]))])
    
    agents = [0,1,2,3]
    elements = [4,5]
    agent_element_pairs = [[0,4], [2,4],[2,5],[3,5]]
    
    all_edges = set()
        
    for n1 in agents:
        for n2 in agents:
            if n1!=n2:
                all_edges.add((n1,n2))
    
    for pair in agent_element_pairs:
        a,b = pair
        all_edges.add((a,b))
        all_edges.add((b,a))
    
    if len(all_edges - user_edge_set) != 0:
        raise AssertionError("Your edge list matrix is missing the following edges {})".format(all_edges - user_edge_set))
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)


def check_data_tensor(tensor):
    x = tensor["x"].cpu().detach().numpy()
    y = tensor["y"].cpu().detach().numpy()
    edge_index = tensor["edge_index"].cpu().detach().numpy()
    
    x_t = [[[ 9, 32,  2,  8, 32,  2,  6, 32,  4,  3, 32,  6],
         [18, 32,  4, 16, 32,  4, 13, 32,  6,  9, 32,  8],
         [38, 30, 16, 38, 22, 16, 38, 14, 16, 38,  6, 16],
         [50, 37,  2, 53, 37,  7, 58, 37, 10, 60, 37, 12],
         [28, 28,  0, 28, 28,  0, 28, 28,  0, 28, 28,  0],
         [42, 42,  0, 42, 42,  0, 42, 42,  0, 42, 42,  0]]]
    
    y_t = [[[1, 0, 0, 0, 0, 0, 0],
         [1, 0, 0, 0, 0, 0, 0],
         [1, 0, 0, 0, 0, 0, 0],
         [1, 0, 0, 0, 0, 0, 0],
         [0, 1, 0, 0, 0, 0, 0],
         [0, 1, 0, 0, 0, 0, 0]]]
    
    edge_index_t = [[[0, 1, 0, 2, 0, 3, 0, 4, 1, 2, 1, 3, 1, 4, 2, 3, 2, 4, 2, 5, 3, 5],
         [1, 0, 2, 0, 3, 0, 4, 0, 2, 1, 3, 1, 4, 1, 3, 2, 4, 2, 5, 2, 5, 3]]]
    
    
    np.testing.assert_array_equal(x,x_t)
    np.testing.assert_array_equal(y,y_t)
    np.testing.assert_array_equal(edge_index,edge_index_t)
                                
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)


def check_trajectory_prediction(get_state_fn):
    states = [1, 2, 3]
    embedding = 5
    def rnn(states, embedding):
        return states[-1] + 1
    output = get_state_fn(rnn, states, embedding)
    if output != 4:
        raise AssertionError("Unexpected output for get_node_current_state")
    
    return display_html("""<div class="alert alert-success">
    <strong>Tests passed!!</strong> </div>""", raw=True)


