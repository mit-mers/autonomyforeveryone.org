---
title: Temporal Networks Part 1
author: Cameron Pittman and Delia Stephens
date: '2022-02-21'
timetofinish: 30 minutes
thumbnail: '/images/STN.png'
binder_url: 'https://mybinder.org/v2/gl/mit-mers%2Fjupyter%2Fsimple-temporal-networks/main'
gitlab_url: 'https://gitlab.com/mit-mers/jupyter/simple-temporal-networks'
---

Temporal networks form the backbone of planning algorithms. They exist to solve the problem of managing relationships between events. The ideas behind temporal networks are very simple. Have you ever planned the details of how you're doing to cook a meal? How about a travel itinerary for a vacation? If so, you've already written a temporal network. In this lesson, we'll help you take your intuition about writing down plans and turn it into formal data structure called a Simple Temporal Network.

Click on the Binder link for this lesson (or click <a href="https://mybinder.org/v2/gl/mit-mers%2Fjupyter%2Fsimple-temporal-networks/main" target="_blank">here</a>) and visit the `lesson1-STNs.ipynb` file.
