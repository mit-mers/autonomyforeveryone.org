// apply global styles
import '@styles/index.css'

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
