# build jupyterlite and update the available notebooks with whatever is in notebooks/

set -e

# clean out the old notebooks
rm -rf public/jl

# build new notebooks
# assume notebooks live in the notebooks/ directory
jupyter lite build \
  --contents notebooks \
  --output-dir public/jl \
  --no-sourcemaps \
  --config jupyter-lite.json

# we don't need the lab interface, so just delete it
rm -rf public/jl/lab
