import numpy as np
import pandas
import asyncio

def returnPosition(action, pos, size):
    # print(action, pos)
    # first check edges, and classify type of edge
    if action == "tag" or pos == "tag":
        return pos

    edge = set()
    if pos <= size - 1:
        edge.add("bottom")
    if pos % size == 0:
        edge.add("left")
    if (pos + 1) % size == 0:
        edge.add("right")
    if (pos >= (size ** 2 - size)) and (pos < size ** 2):
        edge.add("top")
    if action == "up":
        if "top" in edge:
            return pos
        return pos + size
    elif action == "left":
        if "left" in edge:
            return pos
        return pos - 1
    elif action == "right":
        if "right" in edge:
            return pos
        return pos + 1
    elif action == "down":
        if "bottom" in edge:
            return pos
        return pos - size


def makeBoard(size, state):
    board = np.zeros((size, size), dtype=np.uint8).tolist()
    agent1 = state[0]
    agent2 = state[1]
    rabbit = state[2]

    def placeObject(obj, pos, board):
        row = (size - 1) - (pos // size)
        column = pos % size
        if board[row][column] != 0:
            board[row][column] = [*board[row][column], obj]
        else:
            board[row][column] = [obj]

    for obj, pos in (['1', agent1], ['2', agent2], ['r', rabbit]):
        placeObject(obj, pos, board)
    return board


def displayBoard(size, state):
    board = makeBoard(size, state)
    cols = list(str(i) for i in range(size - 1, -1, -1))
    rows = cols
    df = pandas.DataFrame(board, columns=cols[::-1], index=rows)

    print(df)


async def count():
    print("one")
    time.sleep(1)
    return 2

async def main():
    x = await asyncio.gather(count(), count(), count())
    return x


def rabbitAction(state, size):

    a = {
        'up': (0,1),
        'down': (0,-1),
        'left': (-1,0),
        'right': (1,0)
    }

    distance = 0

    positions = []
    for pos in state:
        row = (pos // size)
        column = pos % size
        positions.append((column, row))

    distance = 0
    for action in a:
        rab_pos = np.array(positions[-1])
        transform = a[action]
        rab_pos = rab_pos + transform
        rab_pos[rab_pos<0] = 0
        rab_pos[rab_pos>size] = size
        cur_distance = min([sum(np.absolute(np.array(pos) - rab_pos)) for pos in positions[:-1]])

        if cur_distance > distance:
            candidate = action
            distance = cur_distance

    return candidate

if __name__ == "__main__":
    import time
    s = time.perf_counter()
    x = asyncio.run(main())
    elapsed = time.perf_counter() - s
    print(x, elapsed)

