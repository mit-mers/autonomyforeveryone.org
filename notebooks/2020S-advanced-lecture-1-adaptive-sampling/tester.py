from utils import BanditProblem
import numpy as np
import random
from IPython.core.display import display, HTML

class DeterministicDist():
    """
    A deterministic distribution
    """
    def __init__(self, r):
        self.r = r
        
    def sample(self):
        return self.r

def test_ok():
    """If execution gets to this point, print out a happy message."""
    try:
        from IPython.display import display_html
        display_html("""<div class="alert alert-success">
        <strong>Tests passed!!</strong>
        </div>""", raw=True)
    except:
        print("Tests passed!!")
    
def check_results(reward, policy, exp_reward, exp_policy):
    if reward != exp_reward:
        raise Exception("Wrong total rewards! Output: {0} Should be: {1}.".format(reward, exp_reward))
        
    if policy != exp_policy:
        raise Exception("Wrong policy! Output: {0} Should be: {1}.".format(policy, exp_policy))
    
def test_etc(etc_func):
    """
    Testing function for ETC algorithm
    """
    # Case 1: 2 arm, action 1 best
    reward_dist = [DeterministicDist(0), DeterministicDist(1)]
    bp = BanditProblem(reward_dist)
    reward, best_action = etc_func(1, 10, bp)
    check_results(reward, best_action, 9, 1)
    
    # Case 2: 2 arm, action 0 best
    reward_dist = [DeterministicDist(10), DeterministicDist(1)]
    bp = BanditProblem(reward_dist)
    reward, best_action = etc_func(1, 10, bp)
    check_results(reward, best_action, 91, 0)
    
    # Case 3: 11 arms, random values, 20 rounds
    n = 20
    exp_values = [random.randrange(1, 10, 1) for i in range(10)]
    exp_values.append(12)
    exp_reward = sum(exp_values) + (n-11)*12
    reward_dist = [DeterministicDist(x) for x in exp_values]
    bp = BanditProblem(reward_dist)
    reward, best_action = etc_func(1, n, bp)
    check_results(reward, best_action, exp_reward, 10)
    
    test_ok()
    
def test_ucb(ucb_func):
    """
    Testing function for UCB algorithm
    For now, generates random problems with Deterministic Rewards
        and compares solution code output to user's output
    """
    for test in range(3):
        n = np.random.randint(30,60)
        delta = np.random.rand()
        exp_values = [random.randrange(1, 3, 1) for i in range(2)]
        
        best_index = exp_values.index(random.choice(exp_values))
        exp_values[best_index] = max(exp_values)+20
  
        reward_dist = [DeterministicDist(x) for x in exp_values]
        bp = BanditProblem(reward_dist)
        
        reward, best_action = ucb_func(delta, n, bp)
        
        assert(best_action == best_index)

    test_ok()
    

