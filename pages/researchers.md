## Kirk

Kirk is an executive for RMPL. The following variants of v0.0.4 are available:

* [linux-amd64](https://groups.csail.mit.edu/mers/code/private/kirk/kirk-linux-amd64-v0.0.4.tar.gz)
* [linux-arm64](https://groups.csail.mit.edu/mers/code/private/kirk/kirk-linux-arm64-v0.0.4.tar.gz)
* [darwin-amd64](https://groups.csail.mit.edu/mers/code/private/kirk/kirk-darwin-amd64-v0.0.4.tar.gz)
