import _ from 'lodash';
import Layout from '@components/Layout'

// TODO: also add a "Download all" button. clean up dir first. especially L12 midterm graphs

// these files live at https://drive.google.com/drive/folders/1_roiYvUOdUpsC3o7bHcxpFm2wEkVJNUV
// all doc IDs here should be public
// you can get the doc ID by getting a share link and inspecting the URL
const courseCatalog = [
  {
    title: "Principles of Autonomy and Decision Making",
    semester: "Fall",
    year: "2022",
    lessons: [{
      number: '1',
      title: 'Introduction to Autonomy and Decision Making',
      pptId: '1E_czueTunZHPIpZVES7TGeYNQp4M7BBJ',
      pdfId: '1kIR2bN165RffN6uAGAqZiIBK1OGZX5XM',
      note: ''
    },
    {
      number: '2',
      title: 'Modeling and Uninformed Search',
      pptId: '1O-GD80vfGhomT65M8dIbs6vGYRcTBndt',
      pdfId: '1vAb-E3yFtora2ahmkTgeO8cZJdL4caev',
      note: ''
    },
    {
      number: '3',
      title: 'Path Planning as Informed Search',
      pdfId: '1gdNEnHieaoWkPvmFGKjUUd72XC8VEm-t',
      pptId: '1M4aNs94cIngKTqCqf25lPztmdxZDB61h',
      note: ''
    },
    {
      number: '4',
      title: 'Activity Planning as Heuristic Search',
      pdfId: '1WvBQY_0mlc4WSpn9_0FSsjoOuhHlqL3i',
      pptId: '10TvE9xHAW4T3IbyVcQQpnpeEEDEO348c',
      note: ''
    },
    {
      number: '5',
      title: 'Adversarial Games and Alpha-Beta Search',
      pdfId: '1qKclY8mEcNiWD5d5idD_ldb3F6Y2RlB2',
      pptId: '1QzP0-FkEwljQJvO7T_Wyud2enTm3P7IH',
      note: ''
    },
    {
      number: '6',
      title: "Markov Decision Processes",
      pdfId: '1P1aHRebZ1Qasvj6lnb9bN-7DCIexSFT1',
      pptId: '1cuGMew3z2Fzpn2g9dkzt_LHPruKc-PJ3',
      note: ''
    },
    {
      number: '7',
      title: 'Probabilistic Planning',
      pdfId: '19-lftqzqk1GWJpMD8OeNKgly0TkD6TpG',
      pptId: '1tPRNViQl7HOrsTiktUl6U7bt0Zev0L87',
      note: ''
    },
    {
      number: '8',
      title: 'Hidden Markov Models',
      pdfId: '1AC8AX42Gu1aw42kpE5XxO5yIZNaGiDNu',
      pptId: '1ThV4Wlp08lOxPRg1LjwN2jMl4HHy-Kvd',
      note: ''
    },
    {
      number: '9',
      title: "Propositional Logic: Satisfiability and Entailment",
      pdfId: '1j_IxvSUzP4qr5U0tn-TgG92BOTUY6t2h',
      pptId: '13b3hcanm3KsWN-hNDaEmOTmaio1bSERD',
      note: ''
    },
    {
      number: '10',
      title: "Combining Propositional Inference and Search",
      pdfId: '1k2a_cnDxRah9cU4PwOWOXM2d7sNqcu1k',
      pptId: '1Mzcbn9a3QSvFq_a7UBT7eZ9voc3ubPPe',
      note: ''
    },
    {
      number: '11A',
      title: 'Sampling Based Path Planning',
      pdfId: '1QrzFQhcg2T8sMjY9q0IvjUAYHtinlEhD',
      pptId: '1oHCHhYmt7z5lhm_if8AzSWu3m_Nxjeqs',
      note: ''
    },
    {
      number: '11B',
      title: 'Monte Carlo Tree Search',
      pdfId: '1rj1hiZmvmsmsXB5mVETDdisBNRP6R_Dd',
      pptId: '1oxVWc_YDKcMkDIrXPuMj29SlbM9-Dx64',
      note: ''
    },
    {
      number: '12',
      title: 'Self-Repairing Systems',
      pdfId: '1jIZJ45Ev9oGRe6adjxOxp14xkNPWwVsx',
      pptId: '1-JzjzPAFlKWRBd2loNvhkcg4S1mpnzP_',
      note: ''
    },
    {
      number: '13',
      title: 'Conflict-Directed Search',
      pdfId: '1sOhL556tdaGq3B4GJLM8INIAD4fG2m6M',
      pptId: '1CrIK8z5iVAOXfg4gYcrBh_hgioBSl3CP',
      note: ''
    },
    {
      number: '14',
      title: 'Constraint Programs and Propagation',
      pdfId: '1G0K0Bxdfe0nawKIy7010Pfqy7hq7uCBN',
      pptId: '139HIpe3FeQH54xoMomp8bC7wSyy2qVNY',
      note: ''
    },
    {
      number: '15',
      title: 'Vehicle Routing as Constraint Programs',
      pdfId: '1pQ224GQfsGIwWD8sa-kz5K7XOLTiuL5U',
      pptId: '15Z_UCiys8af1LJiPCcpgrtG3T_ZZD2sK',
      note: ''
    },
    {
      number: '16',
      title: 'Solving Constraint Problems with Search and Elimination',
      pdfId: '1kpXWbNaGXT5aZvHuemEKLgNfYFqTYpvy',
      pptId: '1P-0pM73yFB_aa6UFQ0yh1UfOTMKoyu2q',
      note: ''
    },
    {
      number: '17',
      title: 'Autonomous Ocean Exploration',
      pdfId: null,
      pptId: '13h0YWOGxBGiqI29uZ__H-zTleu6tQSqF',
      note: "Guest lecture by Rich Camilli (WHOI)"
    },
    {
      number: '18',
      title: 'Bayes Nets 1: Exact Inference',
      pdfId: '1wBea59DYsMti0BBiVl4ZQlhSO2LtIha-',
      pptId: '1OaRXTAV4VV74hrw-r_fkrRekbrX0sbOI',
      note: ''
    },
    {
      number: '19',
      title: 'Baytes Nets 2: Approximate Inference',
      pdfId: '13xRjxw-_6Lt82n_1WMSOwKUofccNiAD7',
      pptId: '1-HaoC8YA44mEx_9Nh7BlUlau90tLdKX-',
      note: ''
    },
    {
      number: '20',
      title: 'Temporal Networks',
      pdfId: '1FfF7LsmykxrjZXMDrGE1_sRiKtgbKrJ6',
      pptId: '1S1AWOpXPs3bdBehDKJO2Nu8eY6kANrXc',
      note: ''
    },
    {
      number: '21',
      title: "Mathematical Programming: Modeling, Elimination and Intuitions",
      pdfId: null,
      pptId: '1j5RpWkkEKE-zqm9BHiCpJ0dxoTgoyeIl',
      note: ''
    },
    {
      number: '22',
      title: "Linear Programming: Simplex Method",
      pdfId: '1O2N248fPN6QCvqg-bZjeUJwTGXQyqOIT',
      pptId: '1RFfhLGXAvtdgub_obdVYvwCafTiQGfhg',
      note: ''
    },
    {
      number: '23',
      title: "Mixed Integer Programming with Branch and Bound",
      pdfId: '1mGtsQ7H5_gZeIZvpVRkb_IJd-J5dLTVv',
      pptId: '1ViqYmRRxKOdVnzXrj0bh6qynopFIkv79',
      note: ''
    },
    {
      number: '24',
      title: "Introduction to Convex Optimization",
      pdfId: '12vS4AC5BLAOeoVlCj4_GjxgfxXko_bQW',
      pptId: '1G0zFlWgT9nQVOwMtGhh_CjJYYwmQL1aL',
      note: 'Guest Lecture by Simon Fang (Mobi)'
    },
    {
      number: '25-26',
      title: "Risk-Bounded Planning",
      pdfId: '18IRDQqYCWZx634Pxm9w8IUbsXLdDi3XC',
      pptId: '1OjhAA0kNhPU4MWgYhV5I_omemeBRnKxV',
      note: ''
    },
    {
      number: '27A',
      title: "Course Review",
      pdfId: '1PyyZGol0HIzye9qkQGeB9Y_A1UH0T-YU',
      pptId: '1Dq5tHU53G0CzU3jfSe1VAYvh-4ujSEtw',
      note: ''
    },
    {
      number: '27B',
      title: 'Course Highlights',
      pdfId: '1JW80cZnj7fHkiifYZhD-yWPFZnyzdm9T',
      pptId: '1PHG4eTi4itU-JFvTZpaat0P_Wg9P6pff',
      note: "Supplemental to the Course Review"
    }
    ]
  },
  {
    title: "Cognitive Robotics",
    semester: "Spring",
    year: "2022",
    theme: "Multi-Agent Planning",
    lessons: [
      {
        number: '1',
        title: 'From POMDP to DecPOMDB',
        pptId: '1ShaLzMMdVn5jiF5KUf8peKyGYdZanMXy',
        pdfId: '1Q_amf5CKofozfIWrIa-plOp2KKODInW8',
        notebookPath: '2022S-advanced-lecture-decpomdp/Dec-POMDP.ipynb',
        note: 'Advanced Lecture by students'
      },
      {
        number: '2',
        title: 'Non-Cooperative Games',
        pptId: '1p9LbfyJMrnJqWPhlNDEkPskVUBNCj2Fu',
        pdfId: '1LL9VmPss0JcHTQInhTzOSX6uX5c2-e0_',
        notebookPath: '2022S-advanced-lecture-non-coop-games/MiniPset.ipynb',
        note: 'Advanced Lecture by students. FYI the notebook may not be working'
      },
      {
        number: '3',
        title: 'Trust in Human-Robot Collaboration',
        pptId: '186k2PUAbDleUGnrnkZeLLV9_AAz_DUsq',
        pdfId: '1NnHA-ZtsBOhmfpVleX3HtNGYaQFua1Jm',
        notebookPath: '2022S-advanced-lecture-trust-human-robot',
        note: 'Advanced Lecture by students. FYI the notebook will not run in JupyterLite. Download "2022S-advanced-lecture-trust-human-robot/" and run locally instead'
      },
      {
        number: '4',
        title: 'Applications of Epistemic Logic in Planning',
        pptId: '1UtCDYPu28Ds2KXXcVP2pMvzgKSz1DbMc',
        pdfId: '1wUk7qjzyebrDYXh6hfKtXvmp-xpV3Njh',
        notebookPath: '2022S-advanced-lecture-epistemic-logic-planning',
        note: 'Advanced Lecture by students. FYI the notebook will not run in JupyterLite. Download "2022S-advanced-lecture-epistemic-logic-planning/" and run locally instead'
      },
      {
        number: '5',
        title: 'Multi-Agent Prediction in Autonomous Driving',
        pptId: '1g0s9Hsn96ZOD6nbumxRbP0lrBFXxYAEL',
        pdfId: '1g0s9Hsn96ZOD6nbumxRbP0lrBFXxYAEL',
        notebookPath: '2022S-advanced-lecture-ma-prediction-driving',
        note: 'Advanced Lecture by students. FYI the notebook will not run in JupyterLite. Download "2022S-advanced-lecture-ma-prediction-driving/" and run locally instead'
      },
    ]
  },
  {
    title: "Cognitive Robotics",
    semester: "Spring",
    year: "2020",
    theme: "Planetary Exploration",
    lessons: [
      {
        number: '1',
        title: 'Adaptive Sampling',
        pptId: '1xAdz3a2_5pXT3QAlp54Fe6-QODEDeN-h',
        pdfId: '13eI4JSK0cZQyIsCaBcgQp5n5KDdsHivM',
        notebookPath: '2020S-advanced-lecture-1-adaptive-sampling/minipset-1.ipynb',
        note: 'Advanced Lecture by students'
      },
      {
        number: '2',
        title: 'Semantic Segmentation',
        pptId: '1p9LbfyJMrnJqWPhlNDEkPskVUBNCj2Fu',
        pdfId: '1LL9VmPss0JcHTQInhTzOSX6uX5c2-e0_',
        notebookPath: '2020S-advanced-lecture-2-semantic-segmentation',
        note: 'Advanced Lecture by students. FYI the notebook will not run in JupyterLite. Download "2020S-advanced-lecture-2-semantic-segmentation/" and run locally instead'
      },
      {
        number: '3',
        title: 'Decentralized Planning',
        pptId: '186k2PUAbDleUGnrnkZeLLV9_AAz_DUsq',
        pdfId: '1NnHA-ZtsBOhmfpVleX3HtNGYaQFua1Jm',
        notebookPath: '2020S-advanced-lecture-3-distributed-planning/minipset-3.ipynb',
        note: 'Advanced Lecture by students. FYI the notebook may not be working'
      },
      {
        number: '4',
        title: 'Intent Recognition',
        pptId: '1UtCDYPu28Ds2KXXcVP2pMvzgKSz1DbMc',
        pdfId: '1wUk7qjzyebrDYXh6hfKtXvmp-xpV3Njh',
        notebookPath: '2020S-advanced-lecture-4-intent-recognition',
        note: 'Advanced Lecture by students. FYI the notebook will not run in JupyterLite. Download "2022S-advanced-lecture-epistemic-logic-planning/" and run locally instead'
      },
    ]
  }
]

function makeGDriveLink(fileId) {
  return `https://drive.google.com/uc?export=download&id=${fileId}`;
}

function makeNotebookLink(path) {
  if (_.endsWith(path, ".ipynb")) {
    return `/jl/retro/notebooks/?path=${path}`;
  }

  // it's a path to a directory. JupyterLite is a SPA that doesn't handle direct links to directories :( point to notebook home instead
  return `/jl/retro/tree/index.html`
}

const renderLink = (href, description) => {
  return <a className="underline" download href={href} target="_blank">{description}</a>
}

const renderDocDownload = (docId, description) => {
  if (_.isString(docId) && docId !== "") {
    return renderLink(makeGDriveLink(docId), description);
  }

  return <span>Not available</span>
}

const renderNotebookLink = (path, description) => {
  if (_.isString(path) && path !== "") {
    return renderLink(makeNotebookLink(path), description)
  }

  return <span>Not available</span>
}

export default function References({ siteTitle, description }) {
  const renderLinkTable = ({ lessons, year }) => {
    const areNotebooks = lessons.reduce((prev, lesson) => {
      return (_.isString(lesson.notebookPath) && lesson.notebookPath !== "") || prev;
    }, false);

    return <table className="table-auto">
      <thead>
        <tr>
          <th className="px-4 py-2"></th>
          <th className="px-4 py-2">Lecture Title</th>
          <th className="px-4 py-2">Download PowerPoint</th>
          <th className="px-4 py-2">Download PDF</th>
          {areNotebooks && <th className="px-4 py-2">Jupyter Notebook</th>}
          <th className="px-4 py-2">Note</th>
        </tr>
      </thead>
      <tbody>
        {lessons.map((lecture, index) => {
          return <tr key={`LECTURE_${year}_${index}`} className={index % 2 === 0 ? "bg-gray-200" : ""}>
            <td className="border px-4 py-2">{lecture.number}</td>
            <td className="border px-4 py-2">{lecture.title}</td>
            <td className="border px-4 py-2">{renderDocDownload(lecture.pptId, 'Link to ppt')}</td>
            <td className="border px-4 py-2">{renderDocDownload(lecture.pdfId, 'Link to pdf')}</td>
            {areNotebooks &&
              <td className="border px-4 py-2">{renderNotebookLink(lecture.notebookPath, 'Link to notebook')}</td>
            }
            <td className="border px-4 py-2">{lecture.note || ""}</td>
          </tr>
        })}
      </tbody>
    </table>
  }

  const renderClass = (offering) => {
    const { lessons, semester, title, theme, year } = offering;
    return <div key={`TABLE__${year}-${semester}-${title}`} className="bg-white text-black rounded overflow-hidden shadow-lg p-4 w-100">
      <h2 id={`slides-${year}-${semester}-${title}`}>{title}, {semester} {year}</h2>
      {theme && <p><em>Theme: {theme}</em></p>}
      {renderLinkTable(offering)}
    </div>
  }

  const RenderMaterials = () => {
    return <div>
      {courseCatalog.map(renderClass)}
    </div>
  }

  return (
    <>
      <Layout pageTitle={`${siteTitle} | Resources`} description={description}>
        <h1>Reference Materials</h1>

        <div className="my-4">
          Download course materials from classes offered by the Model-Based Embedded and Robotics Systems group at MIT. Many of the slides include detailed supplemental notes, either suffixed to the slide or in the PowerPoint presenter notes.
        </div>

        <RenderMaterials />
      </Layout>
    </>
  )
}

export async function getStaticProps() {
  const config = await import('../siteconfig.json')

  return {
    props: {
      siteTitle: config.title,
      description: config.description,
    },
  }
}
