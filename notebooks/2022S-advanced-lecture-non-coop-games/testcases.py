# Multiple Choice Tests
# import IPython
# from nose.tools import assert_equal, ok_
import numpy as np
from nose.tools import assert_equal, ok_


def test_ok():
    """ If execution gets to this point, print out a happy message """
    try:
        from IPython.display import display_html
        display_html("""<div class="alert alert-success">
        <strong>Tests passed!!</strong>
        </div>""", raw=True)
    except:
        print("Tests passed!!")

# Multiple choice questions
def test_action1(action):
    assert_equal(action, "D")
    test_ok()

def test_action2(action, expected_utility):
    assert_equal(action, "D")
    assert_equal(expected_utility, 0.66 * 0.75 + 1 * 0.25)
    test_ok()


def test_action3(action, expected_utility):
    assert_equal(action, "U")
    assert_equal(expected_utility, 0.33 * 0.25 + 0.5 * 0.5 + 2 * 0.25)
    test_ok()

# Checking findPolicies function
def test_findPolicies(fn):
    ### Test Case 1 One Possible Clear Move, <Tag, Tag>
    prob_histories_1 = {(1,): 1}
    utilities_1 = {('left', 'left'): {(1,): -0.14071950}, ('left', 'right'): {(1,): 0.01189603266},
                   ('left', 'up'): {(1,): 0.0079728669}, ('left', 'tag'): {(1,): -13.93466028},
                   ('left', 'down'): {(1,): -0.1386750490}, ('right', 'left'): {(1,): 0.01507732280},
                   ('right', 'right'): {(1,): -0.138342892}, ('right', 'up'): {(1,): -0.004528999},
                   ('right', 'tag'): {(1,): -13.7685678164}, ('right', 'down'): {(1,): -0.1373605},
                   ('up', 'left'): {(1,): -0.13703774196550633}, ('up', 'right'): {(1,): -0.13671718540493266},
                   ('up', 'up'): {(1,): 0.1686807595308455}, ('up', 'tag'): {(1,): -15.558272579101985},
                   ('up', 'down'): {(1,): -0.13576884666042613}, ('tag', 'left'): {(1,): 0.0},
                   ('tag', 'right'): {(1,): -3.4098452132276384}, ('tag', 'up'): {(1,): 0.0},
                   ('tag', 'tag'): {(1,): 18.499986534464288}, ('tag', 'down'): {(1,): 0.0},
                   ('down', 'left'): {(1,): 0.0}, ('down', 'right'): {(1,): 0.0}, ('down', 'up'): {(1,): 0.01410965},
                   ('down', 'tag'): {(1,): 0.0}, ('down', 'down'): {(1,): 0.0}}
    sigma = fn(prob_histories_1, utilities_1)
    assert sigma == ('tag', 'tag'), f"Wrong sigma generated. Expected ('tag', 'tag'), got: {sigma} || Test Case 1"
    ### Test Case 2 not (tag,tag) Answer
    prob_histories_2 = {(0,): 1}
    utilities_2 = {('down', 'down'): {(0,): -0.8093875526219916}, ('down', 'tag'): {(0,): -54.8944831434284},
                   ('down', 'up'): {(0,): -0.8055671738933071}, ('down', 'left'): {(0,): -0.6687182321355074},
                   ('down', 'right'): {(0,): -0.5202292115991786}, ('tag', 'down'): {(0,): -22.670707261892275},
                   ('tag', 'tag'): {(0,): -17.93172481153514}, ('tag', 'up'): {(0,): -25.472974736646684},
                   ('tag', 'left'): {(0,): -15.679755417170155}, ('tag', 'right'): {(0,): -11.950997117583528},
                   ('up', 'down'): {(0,): -0.4096086268925718}, ('up', 'tag'): {(0,): -11.472141522892013},
                   ('up', 'up'): {(0,): -0.5161973363225129}, ('up', 'left'): {(0,): -0.3945049025314703},
                   ('up', 'right'): {(0,): -0.47874773231969275}, ('left', 'down'): {(0,): -0.3846656899152313},
                   ('left', 'tag'): {(0,): -11.97124284330227}, ('left', 'up'): {(0,): -0.36911640674305074},
                   ('left', 'left'): {(0,): -0.002890040925482007}, ('left', 'right'): {(0,): -0.35667712262617046},
                   ('right', 'down'): {(0,): -0.3540237631322959}, ('right', 'tag'): {(0,): -9.206671797147653},
                   ('right', 'up'): {(0,): -0.3466300185491921}, ('right', 'left'): {(0,): -0.35183067258768785},
                   ('right', 'right'): {(0,): -0.36173472104206517}}
    sigma = fn(prob_histories_2, utilities_2)
    assert sigma == ('left', 'left'), f"Wrong sigma generated. Expected ('left', 'left'), got: {sigma} || Test Case 2"
    ### Test Case 3 Multiple Histories
    prob_histories_3 = {(0,): 1, (0, 1): 0.5, (0, 0): 0.5, (0, 1, 1): 0.25, (0, 1, 0): 0.25, (0, 0, 1): 0.25,
                        (0, 0, 0): 0.25}
    utilities_3 = {('left', 'left'): {(0, 0, 1): -0.29488391230979427},
                   ('left', 'up'): {(0, 0, 1): -0.2917299829957891},
                   ('left', 'right'): {(0, 0, 1): -0.27472112789737807},
                   ('left', 'tag'): {(0, 0, 1): -25.197631533948478},
                   ('left', 'down'): {(0, 0, 1): -0.24761786378340508},
                   ('up', 'left'): {(0, 0, 1): -0.2195285199793807},
                   ('up', 'up'): {(0, 0, 1): -0.21693045781865616}, ('up', 'right'): {(0, 0, 1): -0.21566554640687682},
                   ('up', 'tag'): {(0, 0, 1): -20.851441405707476}, ('up', 'down'): {(0, 0, 1): -0.11470786693528087},
                   ('right', 'left'): {(0, 0, 1): -0.11414602910706992},
                   ('right', 'up'): {(0, 0, 1): -0.08787495503274936},
                   ('right', 'right'): {(0, 0, 1): -0.08606629658238704},
                   ('right', 'tag'): {(0, 0, 1): -8.199200616907877},
                   ('right', 'down'): {(0, 0, 1): -0.0818545509070058}, ('tag', 'left'): {(0, 0, 1): -8.11774958468199},
                   ('tag', 'up'): {(0, 0, 1): -7.327433054473116}, ('tag', 'right'): {(0, 0, 1): -7.322520259382125},
                   ('tag', 'tag'): {(0, 0, 1): 7.003265056810793}, ('tag', 'down'): {(0, 0, 1): 0.0},
                   ('down', 'left'): {(0, 0, 1): 0.0}, ('down', 'up'): {(0, 0, 1): 0.0},
                   ('down', 'right'): {(0, 0, 1): -0.07720914364821829}, ('down', 'tag'): {(0, 0, 1): -25.0},
                   ('down', 'down'): {(0, 0, 1): 0.0}}
    sigma = fn(prob_histories_3, utilities_3)
    assert sigma == ('tag', 'tag'), f"Wrong sigma generated. Expected ('tag', 'tag'), got: {sigma} || Test Case 3"

    ### Test Case 4 Multiple Histories not (tag,tag)
    prob_histories_4 = {(0,): 1, (0, 1): 0.5, (0, 0): 0.5}
    utilities_4 = {('down', 'down'): {(0, 0): -0.9108808062700839, (0, 1): -0.9108808062700839},
                   ('down', 'tag'): {(0, 0): -52.66410309743502, (0, 1): -52.66410309743502},
                   ('down', 'up'): {(0, 0): -0.6638402780854793, (0, 1): -0.6638402780854793},
                   ('down', 'right'): {(0, 0): -0.5808014301160302, (0, 1): -0.5808014301160302},
                   ('down', 'left'): {(0, 0): -0.5317403846761972, (0, 1): -0.5317403846761972},
                   ('tag', 'down'): {(0, 0): -22.078903026874393, (0, 1): -22.078903026874393},
                   ('tag', 'tag'): {(0, 0): -15.060603793270218, (0, 1): -15.060603793270218},
                   ('tag', 'up'): {(0, 0): -16.320553979060797, (0, 1): -16.320553979060797},
                   ('tag', 'right'): {(0, 0): -19.47769329762046, (0, 1): -19.47769329762046},
                   ('tag', 'left'): {(0, 0): -26.36977774251458, (0, 1): -26.36977774251458},
                   ('up', 'down'): {(0, 0): -0.442359590055646, (0, 1): -0.442359590055646},
                   ('up', 'tag'): {(0, 0): -18.935098232042712, (0, 1): -18.935098232042712},
                   ('up', 'up'): {(0, 0): -0.4134178811634466, (0, 1): -0.4134178811634466},
                   ('up', 'right'): {(0, 0): -0.38780862133755123, (0, 1): -0.38780862133755123},
                   ('up', 'left'): {(0, 0): -0.5031184842483767, (0, 1): -0.5031184842483767},
                   ('right', 'down'): {(0, 0): -0.37509820550344253, (0, 1): -0.37509820550344253},
                   ('right', 'tag'): {(0, 0): -9.248909477239042, (0, 1): -9.248909477239042},
                   ('right', 'up'): {(0, 0): -0.3749625451364532, (0, 1): -0.3749625451364532},
                   ('right', 'right'): {(0, 0): -0.2541584148358684, (0, 1): -0.2541584148358684},
                   ('right', 'left'): {(0, 0): -0.3666186893814976, (0, 1): -0.3666186893814976},
                   ('left', 'down'): {(0, 0): -0.42477575621112806, (0, 1): -0.42477575621112806},
                   ('left', 'tag'): {(0, 0): -12.00029085790775, (0, 1): -12.00029085790775},
                   ('left', 'up'): {(0, 0): -0.36597784610027817, (0, 1): -0.36597784610027817},
                   ('left', 'right'): {(0, 0): -0.4288993503428802, (0, 1): -0.4288993503428802},
                   ('left', 'left'): {(0, 0): -0.5892049864997226, (0, 1): -0.5892049864997226}}
    sigma = fn(prob_histories_4, utilities_4)
    assert sigma == (
    'right', 'right'), f"Wrong sigma generated. Expected ('right', 'right'), got: {sigma} || Test Case 4"
    test_ok()

# Checking rewards
def test_reward(profile, type):
    if type == "movement":
        assert_equal(profile["movement"], 0)

    elif type == "soft":
        assert_equal(profile["movement"] < 0, True)
        assert_equal(profile["success"] > 0, True)
        assert_equal(profile["failure"] < 0, True)
        assert_equal(profile["success"] // np.absolute(profile["failure"]) >= 2, True)

    elif type == "balanced":
        assert_equal(profile["movement"] < 0, True)
        assert_equal(profile["success"] > 0, True)
        assert_equal(profile["failure"] < 0, True)
        assert_equal(np.absolute(profile["failure"]) // profile["success"] >= 5, True)

    test_ok()
