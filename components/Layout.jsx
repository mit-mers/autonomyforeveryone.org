import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'

export default function Layout({ children, pageTitle, description, ...props }) {
  return (
    <div className="mx-auto min-h-screen">
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
        <meta name="Description" content={description}></meta>
        <title>{pageTitle}</title>
      </Head>
      <Header />
      <div className="container content pt-6 pb-6 ml-auto mr-auto"
        style={{
          // header height: 112px. footer height: 96px
          minHeight: "calc(100vh - 112px - 96px)"
        }}>
        {children}
      </div>
      <Footer />
    </div>
  )
}
