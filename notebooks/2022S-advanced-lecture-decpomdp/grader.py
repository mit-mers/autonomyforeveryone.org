from nose.tools import assert_equal, assert_almost_equal, ok_
from utils import *

def test_ok():
    """ If execution gets to this point, print out a happy message """
    try:
        from IPython.display import display_html
        display_html("""<div class="alert alert-success">
        <strong>Tests passed!!</strong>
        </div>""", raw=True)
    except:
        print("Tests passed!!")
        
def check_DecTiger_policy_heuristic(value):
    assert_almost_equal(value, -8)
    
def check_DecTiger_history_probability(prob):
    assert_almost_equal(prob, 0.000253125)
        
def check_DecTiger_MAA_part1(policy):
    answer = {((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')))}
    their_answer = set([tuple([(i,tuple(a[1])) for i, a in enumerate(t)]) for t in get_all_branches((policy,2))])
    
    assert_equal(their_answer, answer)


def check_DecTiger_MAA_part2(policy):
    answer = {((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Left', 'OpenRight', 'Rawr Right'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Left'))),
        ((0, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')),
        (1, ('Listen', 'Rawr Right', 'Listen', 'Rawr Right')))}
    their_answer = set([tuple([(i,tuple(a[1])) for i, a in enumerate(t)]) for t in get_all_branches((policy,2))])
    
    assert_equal(their_answer, answer)