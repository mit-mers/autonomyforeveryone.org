# autonomyforeveryone.org

[autonomyforeveryone.org](autonomyforeveryone.org) outreach website

_Markdown approach inspired by [this demo](https://github.com/cassidoo/next-netlify-blog-starter)_

There are two options for authoring content:
1. Longform text and images written in markdown. These show up on the main autonomyforeveryone.org website
2. [JupyterLite](https://jupyterlite.readthedocs.io/en/latest/) notebooks running in the browser. These are available at [autonomyforeveryone.org/jl](autonomyforeveryone.org/jl) (the `jl` stands for JupyterLite)

We use submodules to include some notebooks. Clone this repo with:

```sh
git clone --recurse-submodules https://gitlab.com/mit-mers/autonomyforeveryone.org
```

## Markdown Lesson Content Management

Lessons are stored as markdown files in [lessons/](./lessons). To create a new lesson, simply create a new markdown file with the frontmatter (header) that you see below! To edit an existing lesson, simply edit the markdown file.

The state of the markdown files in the `live` branch determines what is live on the website.

Not all of the frontmatter shown in the example below is required. Whatever is present will be rendered appropriately in the lesson card.

Sample markdown example with all frontmatter options:
```md
title: 'Cool Cutting Edge Research'
date: '2020-08-31'
author: 'Dr. MERSian'
thumbnail: '/images/picture.jpg'
yt_video_id: 'abcd1234'
binder_url: 'https://mybinder.org/v2/gl/...'
jupyter_url: '/jl/retro/notebooks/...'
gitlab_url: 'https://gitlab.com/foo/bar'
---

Description of the lesson goes here.
```

Looking for a good markdown editor that isn't Emacs or your normal code editor? Here are a few options:

* [Typora](https://typora.io/) - all platforms, free (for now?)
* [Ghostwriter](https://wereturtle.github.io/ghostwriter/) - Linux and Windows, open source, free
* [ia Writer](https://ia.net/writer) - Mac, iOS, Windows, paid
* [Ulysses](https://ulysses.app/) - Mac, iOS, paid

### Draft Lessons

Do you want to write a lesson _without_ it showing up on the main site? Maybe you have a lesson that's not ready for students yet, but you want to publish it to the website so your peers can easily review it. Just add `draft: true` to the frontmatter of the lesson. It won't show up on the home page, but you'll still be able to navigate directly to `/lesson/file_name` to see it.

### Images

Put images in [public/images/](./public/images). The markdown processor does some magic for you that allows you to reference them with just `'/images/image-name.jpg'`.

### LaTeX

Write LaTeX inline using `$y = f(x)$` syntax, and in blocks with double `$$` syntax, eg.

```
$$
y = g(x)
$$
```

### Code

Write code blocks with triple ticks syntax, eg.


    ```lisp
    (format t "this is syntax highlighted lisp")
    ```

FYI, our text line lengths are currently only about 60 chars. If you exceed 60 chars, the code block
will overflow right, meaning users will have to scroll right to see the whole line. Don't be afraid
to use line breaks to keep line lengths short.


### Question Boxes

If you want students to quiz themselves, you can add question boxes to your lessons. The idea is that you'll the students a question and let them think about it. When they want to check themselves, they can click on the box to see the solution. To render one, add a `<QuestionBox>` element to your lesson with `<question>` and `<answer>` children. For example:

```xml
<QuestionBox>
  <question>
    How much wood would a woodchuck chuck if a woodchuck could chuck wood?
  </question>
  <answer>
    All of the wood.
  </answer>
</QuestionBox>
```

#### Limitations and Quirks of Question Boxes

Question boxes don't handle all edge cases for text. Sticking to plain text is a safe bet, but you may also nest elements inside, eg. `<answer>This number has an exponent, 10<sup>99</sup>, and it is a <em>very</em> big number</answer>`. 

* LaTeX and code markdown don't render in question boxes
* Newlines don't render correctly in question boxes, ie. don't try to write separate paragraphs

There's also some weird behavior about question boxes rendering if there is not the proper spacing or text around them. Basically, you should make sure there is an empty line on either side of the question box. When in doubt, it should be bookended by text. For example:

```xml
This is some text explaining that a question is coming. Note there is an empty line below between this text and the question.

<QuestionBox>
  ...
</QuestionBox>

This is some text that sets up the next section. Note that there is an empty line above between the question box and this text.
```

## Jupyter Notebook Content Management

Notebooks are stored as ~.ipynb~ files in [notebooks/](./notebooks).

The recommended workflow is to spin up the site (see above), edit in the browser, download the updated copy, and replace the notebook in [notebooks/](./notebooks) as necessary. The next time you build the notebooks, the edited version will be available.

JupyterLite makes heavy usage of browser local storage. Your local changes will persist across refreshes by default. If you want to see what notebooks look like to new users when they first open them, it's a good idea to keep the "Applications" tab of your browsers dev tools open, which you can use to clear local storage. Clear storage and refresh the page to make sure you're seeing fresh copies of the notebooks on the server.

## Site Development

There are two parts to the application - the main part is a [NextJS](https://nextjs.org/) single page app that reads markdown and generates HTML from it. JupyterLite notebooks are handled separately and built into a static site. We then copy the built JupyterLite site in the [./public](./public) directory so NextJS will host them. There are redirects defined in [./next.config.js](./next.config.js) that make sure links to notebooks work as expected.

### Dependencies

- Node 12.18+. Recommend using [nvm](https://github.com/nvm-sh/nvm) or [nvm-windows](https://github.com/coreybutler/nvm-windows)
- Python 3.10

### Running the Site

**Install all project dependencies**

```sh
npm install

# only required if you want to work on notebooks
# recommend using some kind of virtual environment for python
python -m venv .venv
pip install -r requirements.txt
```

**Run the dev site**

FYI, there won't be any notebooks until you build them first (see below).

```sh
npm run dev
```

The site will be available on port 3000.

**Build and run the production site locally**

In the `build` script in `package.json`, change `NODE_ENV=production` to `NODE_ENV=dev`. Then run

```sh
npm run build && npm run start
```

Don't forget to change the `build` script back before you commit your changes!

The site will be available on port 3000.

**Build JupyterLite notebooks**

```sh
source .venv/bin/activate # or however you want to activate your venv
npm run notebooks
```

This will replace the notebooks hosted on the server with whatever is in [./notebooks](./notebooks). If all you do is rebuild notebooks and refresh the page, you'll still see your local (in browser) changes to the previous notebooks. See note above in the authoring notebooks section about clearing local storage to reset the state of the notebooks in your browser to whatever is being served.

FYI, you can keep the dev site running while you build new notebooks. My workflow has been to keep the dev site running in one terminal and periodically rebuild notebooks in another.

### Styling

The site is setup to be styled with [Tailwind CSS](https://tailwindcss.com/components). Use Tailwind CSS classes whenever possible. Only use custom CSS as a last resort.

To change the theme of the site, including colors, fonts, and spacing options, see [tailwind.config.js](./tailwind.config.js).

To set defaults for different HTML elements, edit `styles/index.css`.

If you want to create custom styles for a component, first ask yourself if you could actually accomplish the same task with existing Tailwind CSS. Then go read the Tailwind CSS [documentation](https://tailwindcss.com/) again. If you can articulate why Tailwind CSS classes cannot meet your needs, then you can go ahead and write custom CSS. We're following [this guide for CSS modules](https://nextjs.org/docs/basic-features/built-in-css-support#adding-component-level-css) to implement component level CSS.

### Debugging

This project is setup for [debugging with VS Code](https://nextjs.org/docs/advanced-features/debugging). It's a two step process.

1. Run the dev site in a terminal somewhere: `npm run dev`
2. Attach the VS Code debugger with F5.

You should be able to set breakpoints and all that fun stuff.
