from itertools import product
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT, TimeoutExpired
from typing import List, Mapping, Union, Sequence

import IPython
from IPython.utils.io import capture_output
from nose.tools import assert_equal, ok_

from pdkb.problems import parse_pdkbddl

PLAN_PATH = "plan.txt"
PLANNER_PATH = "./bfws"


class Predicate:
    def __init__(self, name: str, **arg_domains: Mapping[str, Sequence[str]]):
        self.name = name
        self.arg_domains = {
            arg: tuple(sorted(domain)) for arg, domain in arg_domains.items()
        }

    def __repr__(self):
        if not self.arg_domains:
            return f"{self.name}"
        lines = [f"{self.name}({', '.join(self.arg_domains)})"]
        for arg, domain in self.arg_domains.items():
            lines.append(f"  {arg} in {domain}".replace("'", ""))
        return "\n".join(lines)
    
    def __eq__(self, other):
        if not isinstance(other, Predicate): return False
        if self.name != other.name: return False
        return self.arg_domains == other.arg_domains
    
    def __hash__(self):
        return hash(tuple(self.arg_domains.items()))


class Proposition:
    def __init__(self, name: str):
        self.name = name
    
    def __repr__(self):
        return self.name

    def __eq__(self, other):
        return isinstance(other, Proposition) and self.name == other.name

    def __hash__(self):
        return hash(self.name)


def _to_props(
    maybe_props: Sequence[Union[Proposition, str]]
) -> List[Proposition]:
    return [
        Proposition(name) if isinstance(name, str) else name
        for name in maybe_props
    ]


class Action:
    """
    Represents an action already converted to use propositions and not
    parameters but which may still need ancillary effects added.
    """
    def __init__(
        self,
        name: str,
        preconditions: List[Union[str, Proposition]],
        pos_effects: List[Union[str, Proposition]],
        neg_effects: List[Union[str, Proposition]],
    ):
        self.name = name
        self.preconditions = _to_props(preconditions)
        self.pos_effects = _to_props(pos_effects)
        self.neg_effects = _to_props(neg_effects)
    
    def __repr__(self):
        return (
            f"{self.name}\n"
            f"  Precond: {self.preconditions}\n"
            f"  Eff+: {self.pos_effects}\n"
            f"  Eff-: {self.neg_effects}\n"
        )

    def __eq__(self, other):
        if not isinstance(other, Action):
            return False
        return (
            self.name == other.name and
            set(self.preconditions) == set(other.preconditions) and
            set(self.pos_effects) == set(other.pos_effects) and
            set(self.neg_effects) == set(other.neg_effects)
        )


def convert_pdkbdll_to_pddl(
    pdkbdll_path: str,
    pddl_domain_path: str="domain.pddl",
    pddl_problem_path: str="problem.pddl",
) -> None:
    """
    Convert a PDKBDLL (epistemic logic) planning problem to a PDDL (propositional logic) one.
    
    This creates two files pddl files with the given names
    """
    problem = parse_pdkbddl(pdkbdll_path)
    problem.preprocess()
    with capture_output() as _:
        # this one prints a few things that we want to suppress
        Path(pddl_domain_path).write_text(problem.domain.pddl())
    Path(pddl_problem_path).write_text(problem.pddl())


def _get_plan_from_file(plan_path: str) -> List[str]:
    """
    :param plan_path: path to plan file. This file will be deleted after
      the plan is extracted.
    The file execution.details in the same directory as the plan file
    will also be deleted.
    """
    plan_file = Path(plan_path)
    raw_plan = plan_file.read_text().strip().split("\n")
    plan = []
    for action in raw_plan:
        action = action.replace("(", "").replace(")", "").strip()
        if "_" in action:
            func, *args = action.split("_")
            action = f"{func}({', '.join(args)})"
        plan.append(action)
    plan_file.unlink()
    try:
        (plan_file.parent / "execution.details").unlink()
    except FileNotFoundError:
        pass
    return plan


def eplan(domain_path: str="domain.pddl", problem_path: str="problem.pddl", timeout: float=10, complete_search: bool=False) -> List[str]:
    """
    This uses the staged BFWS approach from
    https://github.com/QuMuLab/pdkb-planning/blob/master/pdkb/planners/staged_bfws.py.

    :param domain_path: path to the domain PDDL file
    :param problem_path: path to the problem PDDL file
    :param timeout: maximum number of seconds to run the planner
    """
    cmd = [
        PLANNER_PATH,
        "--domain", domain_path,
        "--problem", problem_path,
        "--output", PLAN_PATH,
    ]
    if ...:
        cmd.extend(["--1-BFWS", "1"])
    else:
        cmd.extend(["--BFWS-f5", "1", "--max_novelty", "1"])
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, text=True)
    try:
        stdout, stderr = p.communicate(timeout=timeout)
    except TimeoutExpired:
        p.kill()
        stdout, stderr = p.communicate()
    if stderr:
        raise Exception(f"Exception thrown by planner!\n{stderr}")

    try:
        plan = _get_plan_from_file(PLAN_PATH)
    except FileNotFoundError:
        if complete_search:
            raise RuntimeError("Unable to find a plan for this problem!")
        plan = eplan(domain_path, problem_path, timeout, complete_search=True)
    if not plan:
        raise RuntimeError(
            "Plan was empty; either the initial state satisfies the goal "
            "or there was an error."
        )
    return plan

# AUTOGRADING


# copied from pset 4's utils.py
def test_ok():
    """ If execution gets to this point, print out a happy message """
    try:
        from IPython.display import display_html
        display_html("""<div class="alert alert-success">
        <strong>Tests passed!!</strong>
        </div>""", raw=True)
    except:
        print("Tests passed!!")


def test_ak_predicate_to_propositions(func):
    soln = {Proposition("secret"), Proposition("not_secret")}
    assert_equal(set(func(Predicate("secret"))), soln)
    
    locations = ("l1", "l2", "l3", "l4")
    successor = Predicate("succ", l1=locations, l2=locations)
    soln = set([Proposition(x) for x in [
        "succ_l1_l1",
        "not_succ_l1_l1",
        "succ_l1_l2",
        "not_succ_l1_l2",
        "succ_l1_l3",
        "not_succ_l1_l3",
        "succ_l1_l4",
        "not_succ_l1_l4",
        "succ_l2_l1",
        "not_succ_l2_l1",
        "succ_l2_l2",
        "not_succ_l2_l2",
        "succ_l2_l3",
        "not_succ_l2_l3",
        "succ_l2_l4",
        "not_succ_l2_l4",
        "succ_l3_l1",
        "not_succ_l3_l1",
        "succ_l3_l2",
        "not_succ_l3_l2",
        "succ_l3_l3",
        "not_succ_l3_l3",
        "succ_l3_l4",
        "not_succ_l3_l4",
        "succ_l4_l1",
        "not_succ_l4_l1",
        "succ_l4_l2",
        "not_succ_l4_l2",
        "succ_l4_l3",
        "not_succ_l4_l3",
        "succ_l4_l4",
        "not_succ_l4_l4",
    ]])
    assert_equal(set(func(successor)), soln)


def test_belief_predicate_to_propositions(func):
    pred = Predicate("secret")
    agents = ["a", "b"]
    max_belief_nesting = 1
    soln = set([Proposition(x) for x in [
        "secret",
        "not_secret",
        "Ba_secret",
        "Ba_not_secret",
        "not_Ba_secret",
        "not_Ba_not_secret",
        "Bb_secret",
        "Bb_not_secret",
        "not_Bb_secret",
        "not_Bb_not_secret",
    ]])
    assert_equal(set(func(pred, agents, max_belief_nesting)), soln)

    pred = Predicate("yummy", cooked=["cooked", "raw"], food=["fish", "steak"])
    agents = ["dog", "cat"]
    max_belief_nesting = 2
    soln = set([Proposition(x) for x in [
        "yummy_cooked_fish",
        "not_yummy_cooked_fish",
        "yummy_cooked_steak",
        "not_yummy_cooked_steak",
        "yummy_raw_fish",
        "not_yummy_raw_fish",
        "yummy_raw_steak",
        "not_yummy_raw_steak",
        "Bdog_yummy_cooked_fish",
        "Bdog_not_yummy_cooked_fish",
        "Bdog_yummy_cooked_steak",
        "Bdog_not_yummy_cooked_steak",
        "Bdog_yummy_raw_fish",
        "Bdog_not_yummy_raw_fish",
        "Bdog_yummy_raw_steak",
        "Bdog_not_yummy_raw_steak",
        "not_Bdog_yummy_cooked_fish",
        "not_Bdog_not_yummy_cooked_fish",
        "not_Bdog_yummy_cooked_steak",
        "not_Bdog_not_yummy_cooked_steak",
        "not_Bdog_yummy_raw_fish",
        "not_Bdog_not_yummy_raw_fish",
        "not_Bdog_yummy_raw_steak",
        "not_Bdog_not_yummy_raw_steak",
        "Bcat_yummy_cooked_fish",
        "Bcat_not_yummy_cooked_fish",
        "Bcat_yummy_cooked_steak",
        "Bcat_not_yummy_cooked_steak",
        "Bcat_yummy_raw_fish",
        "Bcat_not_yummy_raw_fish",
        "Bcat_yummy_raw_steak",
        "Bcat_not_yummy_raw_steak",
        "not_Bcat_yummy_cooked_fish",
        "not_Bcat_not_yummy_cooked_fish",
        "not_Bcat_yummy_cooked_steak",
        "not_Bcat_not_yummy_cooked_steak",
        "not_Bcat_yummy_raw_fish",
        "not_Bcat_not_yummy_raw_fish",
        "not_Bcat_yummy_raw_steak",
        "not_Bcat_not_yummy_raw_steak",
        "Bdog_Bdog_yummy_cooked_fish",
        "Bdog_Bdog_not_yummy_cooked_fish",
        "Bdog_Bdog_yummy_cooked_steak",
        "Bdog_Bdog_not_yummy_cooked_steak",
        "Bdog_Bdog_yummy_raw_fish",
        "Bdog_Bdog_not_yummy_raw_fish",
        "Bdog_Bdog_yummy_raw_steak",
        "Bdog_Bdog_not_yummy_raw_steak",
        "Bdog_not_Bdog_yummy_cooked_fish",
        "Bdog_not_Bdog_not_yummy_cooked_fish",
        "Bdog_not_Bdog_yummy_cooked_steak",
        "Bdog_not_Bdog_not_yummy_cooked_steak",
        "Bdog_not_Bdog_yummy_raw_fish",
        "Bdog_not_Bdog_not_yummy_raw_fish",
        "Bdog_not_Bdog_yummy_raw_steak",
        "Bdog_not_Bdog_not_yummy_raw_steak",
        "Bdog_Bcat_yummy_cooked_fish",
        "Bdog_Bcat_not_yummy_cooked_fish",
        "Bdog_Bcat_yummy_cooked_steak",
        "Bdog_Bcat_not_yummy_cooked_steak",
        "Bdog_Bcat_yummy_raw_fish",
        "Bdog_Bcat_not_yummy_raw_fish",
        "Bdog_Bcat_yummy_raw_steak",
        "Bdog_Bcat_not_yummy_raw_steak",
        "Bdog_not_Bcat_yummy_cooked_fish",
        "Bdog_not_Bcat_not_yummy_cooked_fish",
        "Bdog_not_Bcat_yummy_cooked_steak",
        "Bdog_not_Bcat_not_yummy_cooked_steak",
        "Bdog_not_Bcat_yummy_raw_fish",
        "Bdog_not_Bcat_not_yummy_raw_fish",
        "Bdog_not_Bcat_yummy_raw_steak",
        "Bdog_not_Bcat_not_yummy_raw_steak",
        "not_Bdog_Bdog_yummy_cooked_fish",
        "not_Bdog_Bdog_not_yummy_cooked_fish",
        "not_Bdog_Bdog_yummy_cooked_steak",
        "not_Bdog_Bdog_not_yummy_cooked_steak",
        "not_Bdog_Bdog_yummy_raw_fish",
        "not_Bdog_Bdog_not_yummy_raw_fish",
        "not_Bdog_Bdog_yummy_raw_steak",
        "not_Bdog_Bdog_not_yummy_raw_steak",
        "not_Bdog_not_Bdog_yummy_cooked_fish",
        "not_Bdog_not_Bdog_not_yummy_cooked_fish",
        "not_Bdog_not_Bdog_yummy_cooked_steak",
        "not_Bdog_not_Bdog_not_yummy_cooked_steak",
        "not_Bdog_not_Bdog_yummy_raw_fish",
        "not_Bdog_not_Bdog_not_yummy_raw_fish",
        "not_Bdog_not_Bdog_yummy_raw_steak",
        "not_Bdog_not_Bdog_not_yummy_raw_steak",
        "not_Bdog_Bcat_yummy_cooked_fish",
        "not_Bdog_Bcat_not_yummy_cooked_fish",
        "not_Bdog_Bcat_yummy_cooked_steak",
        "not_Bdog_Bcat_not_yummy_cooked_steak",
        "not_Bdog_Bcat_yummy_raw_fish",
        "not_Bdog_Bcat_not_yummy_raw_fish",
        "not_Bdog_Bcat_yummy_raw_steak",
        "not_Bdog_Bcat_not_yummy_raw_steak",
        "not_Bdog_not_Bcat_yummy_cooked_fish",
        "not_Bdog_not_Bcat_not_yummy_cooked_fish",
        "not_Bdog_not_Bcat_yummy_cooked_steak",
        "not_Bdog_not_Bcat_not_yummy_cooked_steak",
        "not_Bdog_not_Bcat_yummy_raw_fish",
        "not_Bdog_not_Bcat_not_yummy_raw_fish",
        "not_Bdog_not_Bcat_yummy_raw_steak",
        "not_Bdog_not_Bcat_not_yummy_raw_steak",
        "Bcat_Bdog_yummy_cooked_fish",
        "Bcat_Bdog_not_yummy_cooked_fish",
        "Bcat_Bdog_yummy_cooked_steak",
        "Bcat_Bdog_not_yummy_cooked_steak",
        "Bcat_Bdog_yummy_raw_fish",
        "Bcat_Bdog_not_yummy_raw_fish",
        "Bcat_Bdog_yummy_raw_steak",
        "Bcat_Bdog_not_yummy_raw_steak",
        "Bcat_not_Bdog_yummy_cooked_fish",
        "Bcat_not_Bdog_not_yummy_cooked_fish",
        "Bcat_not_Bdog_yummy_cooked_steak",
        "Bcat_not_Bdog_not_yummy_cooked_steak",
        "Bcat_not_Bdog_yummy_raw_fish",
        "Bcat_not_Bdog_not_yummy_raw_fish",
        "Bcat_not_Bdog_yummy_raw_steak",
        "Bcat_not_Bdog_not_yummy_raw_steak",
        "Bcat_Bcat_yummy_cooked_fish",
        "Bcat_Bcat_not_yummy_cooked_fish",
        "Bcat_Bcat_yummy_cooked_steak",
        "Bcat_Bcat_not_yummy_cooked_steak",
        "Bcat_Bcat_yummy_raw_fish",
        "Bcat_Bcat_not_yummy_raw_fish",
        "Bcat_Bcat_yummy_raw_steak",
        "Bcat_Bcat_not_yummy_raw_steak",
        "Bcat_not_Bcat_yummy_cooked_fish",
        "Bcat_not_Bcat_not_yummy_cooked_fish",
        "Bcat_not_Bcat_yummy_cooked_steak",
        "Bcat_not_Bcat_not_yummy_cooked_steak",
        "Bcat_not_Bcat_yummy_raw_fish",
        "Bcat_not_Bcat_not_yummy_raw_fish",
        "Bcat_not_Bcat_yummy_raw_steak",
        "Bcat_not_Bcat_not_yummy_raw_steak",
        "not_Bcat_Bdog_yummy_cooked_fish",
        "not_Bcat_Bdog_not_yummy_cooked_fish",
        "not_Bcat_Bdog_yummy_cooked_steak",
        "not_Bcat_Bdog_not_yummy_cooked_steak",
        "not_Bcat_Bdog_yummy_raw_fish",
        "not_Bcat_Bdog_not_yummy_raw_fish",
        "not_Bcat_Bdog_yummy_raw_steak",
        "not_Bcat_Bdog_not_yummy_raw_steak",
        "not_Bcat_not_Bdog_yummy_cooked_fish",
        "not_Bcat_not_Bdog_not_yummy_cooked_fish",
        "not_Bcat_not_Bdog_yummy_cooked_steak",
        "not_Bcat_not_Bdog_not_yummy_cooked_steak",
        "not_Bcat_not_Bdog_yummy_raw_fish",
        "not_Bcat_not_Bdog_not_yummy_raw_fish",
        "not_Bcat_not_Bdog_yummy_raw_steak",
        "not_Bcat_not_Bdog_not_yummy_raw_steak",
        "not_Bcat_Bcat_yummy_cooked_fish",
        "not_Bcat_Bcat_not_yummy_cooked_fish",
        "not_Bcat_Bcat_yummy_cooked_steak",
        "not_Bcat_Bcat_not_yummy_cooked_steak",
        "not_Bcat_Bcat_yummy_raw_fish",
        "not_Bcat_Bcat_not_yummy_raw_fish",
        "not_Bcat_Bcat_yummy_raw_steak",
        "not_Bcat_Bcat_not_yummy_raw_steak",
        "not_Bcat_not_Bcat_yummy_cooked_fish",
        "not_Bcat_not_Bcat_not_yummy_cooked_fish",
        "not_Bcat_not_Bcat_yummy_cooked_steak",
        "not_Bcat_not_Bcat_not_yummy_cooked_steak",
        "not_Bcat_not_Bcat_yummy_raw_fish",
        "not_Bcat_not_Bcat_not_yummy_raw_fish",
        "not_Bcat_not_Bcat_yummy_raw_steak",
        "not_Bcat_not_Bcat_not_yummy_raw_steak",
    ]])
    assert_equal(set(func(pred, agents, max_belief_nesting)), soln)


def test_negation_removal(func):
    move_l1_l2 = Action(
        "move_l1_l2",
        ["at_l1", "succ_l1_l2"],
        ["at_l2", "not_at_l1"],
        [],
    )
    move_l1_l2_negation_removal = Action(
        "move_l1_l2",
        ["at_l1", "succ_l1_l2"],
        ["at_l2", "not_at_l1"],
        ["not_at_l2", "at_l1"],
    )
    func(move_l1_l2)
    assert_equal(move_l1_l2, move_l1_l2_negation_removal)


def test_coin_in_box(fname: str="coin_in_box.pdkbdll", print_plan: bool=False):
    convert_pdkbdll_to_pddl(fname)
    plan = eplan()
    if print_plan:
        print("Plan:")
        for action in plan:
            print(action)

    if plan.pop() != "ANNOUNCE-HEADS(A, B)":
        raise RuntimeError("Expected A to announce-heads to B as final action.")
    if plan.pop() != "PEEK(A)":
        raise RuntimeError("Expected A to peek as penultimate action.")
    try:
        plan.remove("DISTRACT(C, B)")
    except ValueError:
        try:
            plan.remove("DISTRACT(A, B)")
        except ValueError:
            raise RuntimeError("Expected B to be distracted.")

    try:
        unlock_idx = plan.index("UNLOCK(A)")
        plan.pop(unlock_idx)
    except ValueError:
        raise RuntimeError("Expected A to unlock the box.")

    try:
        distract_idx = plan[:unlock_idx].index("DISTRACT(B, C)")
    except ValueError:
        try:
            distract_idx = plan[:unlock_idx].find("DISTRACT(A, C)")
        except ValueError:
            raise RuntimeError("Expected C to be distracted before A unlocks.")

    plan.pop(distract_idx)
    if plan:
        raise RuntimeError("Found unexpected actions:", plan)