import _ from 'lodash'
import matter from 'gray-matter'
import Badges from '@components/Badges'
import MarkdownRender from '@components/MarkdownRender'
import Layout from '@components/Layout'
import getSlugs from '@utils/getSlugs'

export default function BlogPost({ siteTitle, frontmatter, markdownBody, lessonName }) {
  if (!frontmatter) {
    return <></>
  }

  const title = _.get(frontmatter, 'title', null);
  const binder_url = _.get(frontmatter, 'binder_url', null);
  const gitlab_url = _.get(frontmatter, 'gitlab_url', null);
  const jupyter_url = _.get(frontmatter, 'jupyter_url', null);
  const thumbnail = _.get(frontmatter, 'thumbnail', null);
  const ytVideoID = _.get(frontmatter, 'yt_video_id', null);

  const hasBadges = jupyter_url || gitlab_url || binder_url

  const gitlabLink = `https://gitlab.com/mit-mers/autonomyforeveryone.org/-/blob/live/lessons/${lessonName}.md`;

  return (
    <div>
      <Layout pageTitle={`${siteTitle} | ${title}`}>
        <article className="w-100 max-w-screen-lg m-auto">
          <div className="bg-white text-black rounded overflow-hidden shadow-lg p-4 w-100">
            <h1>{title}</h1>
            { hasBadges &&
              <Badges binderURL={binder_url} gitlabURL={gitlab_url} jupyterURL={jupyter_url} />
            }
            <div className="flex lg:flex-row flex-col justify-between">
              <div className="pb-2">
                <img className="max-w-sm" src={thumbnail} alt="Lesson thumbnail" />
              </div>
              <div>
                { ytVideoID &&
                  <div className="h-yt-embed mr-2">
                    <iframe width="560" height="315"
                            src={`https://www.youtube.com/embed/${ytVideoID}`}
                            frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen>
                    </iframe>
                  </div>
                }
              </div>
            </div>
            <div className="lg:mr-2 max-w-2xl mt-4">
              <div className="mt-4 mb-4" >
                <p className="italic text-sm text-indent-2">By {frontmatter.author}. It takes approximately {frontmatter.timetofinish} to complete this lesson.</p>
              </div>
              <MarkdownRender className="prose-md" source={markdownBody} />
              <hr />
              <div>Is there something you'd like to change about this lesson? You can because it's open source! Here is a link to the lesson's markdown on gitlab.com: <a className="text-blue-500 underline" href={gitlabLink} target="_blank">link</a>. Here are instructions for forking and submitting a merge request: <a className="text-blue-500 underline" href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html" target="_blank">link</a>.</div>
            </div>
          </div>
        </article>
      </Layout>
    </div >
  )
}

export async function getStaticProps({ ...ctx }) {
  const { lessonname } = ctx.params

  const content = await import(`../../lessons/${lessonname}.md`)
  const config = await import(`../../siteconfig.json`)
  const data = matter(content.default)

  return {
    props: {
      lessonName: lessonname,
      siteTitle: config.title,
      frontmatter: data.data,
      markdownBody: data.content,
    },
  }
}

export async function getStaticPaths() {
  const blogSlugs = ((context) => {
    return getSlugs(context)
  })(require.context('../../lessons', true, /\.md$/))

  const paths = blogSlugs.map((slug) => `/lesson/${slug}`)

  return {
    paths, // An array of path names, and any params
    fallback: false, // so that 404s properly appear if something's not matching
  }
}
