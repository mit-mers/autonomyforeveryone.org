const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  target: 'serverless',
  assetPrefix: isProd ? 'https://www.autonomyforeveryone.org' : '',
  webpack: function (config) {
    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader',
    })
    return config
  },
  env: {
    NEXT_PUBLIC_KIRK_URL: "https://us-central1-mers-research.cloudfunctions.net/kirk-linux-amd64"
  },
  redirects: async () => {
    // 'jl' means "jupyterlite"
    return [
      // next.js uses literal file handling in public/, so we need an explicit index.html file
      {
        source: "/jl",
        destination: "/jl/index.html",
        permanent: true
      },
      {
        source: "/jl/retro",
        destination: "/jl/retro/index.html",
        permanent: true
      },
      {
        source: "/jl/retro/notebooks",
        destination: "/jl/retro/notebooks/index.html",
        permanent: true
      },
      // redirect lab links (which users shouldn't see anyway)
      {
        source: "/jl/lab",
        destination: "/jl/retro/index.html",
        permanent: true
      },
      {
        source: "/jl/lab/index.html",
        destination: "/jl/retro/index.html",
        permanent: true
      },
    ]
  }
}
