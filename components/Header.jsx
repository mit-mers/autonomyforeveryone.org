import Link from 'next/link'

export default function Header() {
  return (
    <nav className="flex items-center justify-between flex-wrap bg-gray-900 text-white p-6">
      <div className="flex items-center flex-shrink-0 text-white mr-6">
        <span className="font-semibold text-3xl tracking-tight">
          <Link href="/"><a>Autonomy for Everyone</a></Link>
        </span>
      </div>
      <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
        <div className="text-xl lg:flex-grow">
          <span className="block mt-4 lg:inline-block lg:mt-0 mr-4">
            <Link href="/jl">
              <a>Notebooks</a>
            </Link>
          </span>
          <span className="block mt-4 lg:inline-block lg:mt-0 mr-4">
            <Link href="/download">
              <a>Downloads</a>
            </Link>
          </span>
          <span className="block mt-4 lg:inline-block lg:mt-0 mr-4">
            <Link href="/references">
              <a>Reference Materials</a>
            </Link>
          </span>
          <span className="block mt-4 lg:inline-block lg:mt-0 mr-4">
            <Link href="/about">
              <a>About</a>
            </Link>
          </span>
        </div>
      </div>
      <a href="https://www.csail.mit.edu/research/model-based-embedded-and-robotics-systems-group" target="_blank">
        <img src="/images/mers-4.png" style={{ height: '64px' }} />
      </a>
    </nav>
  )
}
