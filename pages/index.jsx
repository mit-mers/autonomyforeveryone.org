import Layout from '@components/Layout'
import LessonList from '@components/LessonList'
import getLessons from '@utils/getLessons'

const Index = ({ lessons, title, description, ...props }) => {
  return (
    <>
      <Layout pageTitle={title} description={description}>
        <main>
          <LessonList lessons={lessons} />
        </main>
      </Layout>
    </>
  )
}

export default Index

export async function getStaticProps() {
  const configData = await import(`../siteconfig.json`)

  const lessons = ((context) => {
    return getLessons(context)
  })(require.context('../lessons/', true, /lessons\/.*\.md$/))

  // TODO: check that lessons have all the frontmatter required

  return {
    props: {
      lessons,
      title: configData.default.title,
      description: configData.default.description,
    },
  }
}
