# Natural Language imports
# http://www.nltk.org/
import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('tagsets')

# PyTorch
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

# Time
import time

# nose
from nose.tools import assert_equal, ok_

def test_ok():
    """ If execution gets to this point, print out a happy message """
    try:
        from IPython.display import display_html
        display_html("""<div class="alert alert-success">
        <strong>Tests passed!!</strong>
        </div>""", raw=True)
    except:
        print("Tests passed!!")

class Node:
    def __init__(self, name):
        self.name = name
        self.sem_class = None
        self.state = []
        
        self.influenced_by = []
        self.influences = []
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name

def check_state(scene):
    assert_equal(len(scene), 3)
    red, green, blue = scene
    
    assert_equal(red.state, [(-1,0), (0,0), (1,0)])
    assert_equal(green.state, [(0.5,5),(0.5,2),(0.5,-1)])
    assert_equal(blue.state, [(1,1), (1,0), (1,-1)])
    
    test_ok()

def check_sem_class(scene):
    assert_equal(len(scene), 3)
    red, green, blue = scene
    
    assert_equal(red.sem_class, 'pedestrian')
    assert_equal(green.sem_class, 'vehicle')
    assert_equal(blue.sem_class, 'pedestrian')
    
    test_ok()
    
def check_generate_edges(fn):
    red = Node('red')
    green = Node('green')
    blue = Node('blue')

    scene = [red, green, blue]
    
    red.state = [(-1,0), (0,0), (1,0)]
    green.state = [(0.5,5),(0.5,2),(0.5,-1)]
    blue.state = [(1,1), (1,0), (1,-1)]
    
    red.sem_class = 'pedestrian'
    green.sem_class = 'vehicle'
    blue.sem_class = 'pedestrian'
    
    radius_parameters = {
        'pedestrian': 1,
        'vehicle': 5
    }
    
    fn(scene, radius_parameters)
    
    assert_equal(red.influenced_by, [[], [blue], [blue]])
    assert_equal(green.influenced_by, [[blue], [red, blue], [red, blue]])
    assert_equal(blue.influenced_by, [[], [red], [red, green]])
    
    assert_equal(red.influences, [[], [green, blue], [green, blue]])
    assert_equal(green.influences, [[], [], [blue]])
    assert_equal(blue.influences, [[green], [red, green], [red, green]])
    
    test_ok()

# useful utility function, inverse of built in zip
def unzip(lst):
    return ([ i for i, j in lst ], 
            [ j for i, j in lst ])

# useful utility function, flips key values in dict
def reverse_dict(d):
    return {v: k for k,v in d.items()}

# friendly wrapper to return text from littleprince.txt
def load_littleprince():
    with open('littleprince.txt','r') as READ:
        text = READ.read()
    return text

# friendly wrapper around nltk.word_tokenize to split on words
def split_words(text):
    return [word for word in nltk.word_tokenize(text)]

# friendly wrapper around nltk.pos_tag to perform the word parsing.
def pos_tag(text):
    words = split_words(text)
    tagged = nltk.pos_tag(words) # defaults to Penn Treebank tagset
    return tagged

# Splits training_data into "sentence-like" blocks.
# Specifically, tries to split at sentence boundaries.
# Will keep quoted blocks as "one sentence" to keep open/close quotes together.
def sentence_split(tagged_corpus):
    sentences = []
    curr = []
    quote_depth = 0
    skip = False
    
    # step through ever tagged word
    for i,(word,tag) in enumerate(tagged_corpus):
        # skip if close quote included in last sentence
        if skip:
            skip = False
            continue
            
        # append tagged word to current sentence
        curr.append( (word,tag) )
        
        # increase quote depth
        if tag == "``":
            quote_depth += 1
        
        # decrease quote depth
        if tag == "''":
            quote_depth -= 1
        
        # split on sentance boundaries
        if tag == '.':
            # if we're inside a quote
            if quote_depth > 0:
                # if word after sentence end is a close quote
                if tagged_corpus[i+1][1] == "''":
                    # add quote to current completed sentence
                    curr.append( tagged_corpus[i+1] )
                    quote_depth -= 1
                    skip = True
                    # if next word is not capitalized (useful if punctuation was ! or ?)
                    if tagged_corpus[i+2][0][0] in 'abcdefghijklmnopqrstuvwxyz':
                        # sentence isn't over
                        continue
                else:
                    # keep going until we find sentence end outside of quote
                    # or sentence end at close quote
                    continue
            
            # add completed sentence to sentences and clear curr
            sentences.append(curr)
            curr = []
            quote_depth = 0
    
    # return completed sentences
    return sentences

# Given a long block of text, # splits it into:
# -- training_data: a list of tuples (word,tag)
# -- word_encoding: a dictionary of (word: model index)
# -- tag_encoding: a dictionary of (tag: model index)
def create_training(text):
    # replaces new lines and splits on words
    words_corpus = split_words(text.replace('\n', ' '))
    
    # tags words
    tagged_corpus = nltk.pos_tag(words_corpus)
    
    # split into sentences
    sentences = sentence_split(tagged_corpus)
    
    # create training_data
    training_data = [unzip(sentence) for sentence in sentences]
    
    # unzip words tags
    words,tags = unzip(tagged_corpus)
    
    # create word_encoding
    sorted_words = sorted(list(set(words)))
    word_encoding = dict((word,i) for i,word in enumerate(sorted_words))
    
    # create tag_encoding
    sorted_tags = sorted(list(set(tags)))
    tag_encoding = dict((tag,i) for i,tag in enumerate(sorted_tags))
    
    # return components
    return training_data, word_encoding, tag_encoding

# Class that contains the structure of our LSTM Tagger
class LSTMTagger(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, vocab_size, tagset_size):
        super(LSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim

        self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)

        # The LSTM takes word embeddings as inputs, and outputs hidden states
        # with dimensionality hidden_dim.
        self.lstm = nn.LSTM(embedding_dim, hidden_dim)

        # The linear layer that maps from hidden state space to tag space
        self.hidden2tag = nn.Linear(hidden_dim, tagset_size)

    def forward(self, sentence):
        embeds = self.word_embeddings(sentence)
        lstm_out, _ = self.lstm(embeds.view(len(sentence), 1, -1))
        tag_space = self.hidden2tag(lstm_out.view(len(sentence), -1))
        tag_scores = F.log_softmax(tag_space, dim=1)
        return tag_scores

# Takes a model, a set of validate_data (same structure as training_data), a
# word_encoding dictionary, and a tag_encoding dictionary.
# Runs the forward process on the words in validate_data and compares to the
# results in the result provided in the tags of validate_data.
# Prints accuracy of validation test.
def model_validate(model, validate_data, word_encoding, tag_encoding):
    correct = 0
    total = 0
    
    validate_sentences, validate_tags = unzip(validate_data)
    for i,sentence in enumerate(validate_sentences):
        tag_scores = model_run_raw(model, sentence, word_encoding, tag_encoding)
        tag_results = tag_scores_to_results(tag_scores, tag_encoding)
        
        results = [x == y for x,y in zip(validate_tags[i], tag_results)]
        correct += results.count(True)
        total += len(results)
    
    print('Validation: %d/%d (%0.02f%%)' % (correct, total, correct/total*100))
    
# Takes a model, a text string, a word_encoding dictionary, and a tag_encoding dictionary.
# Runs nltk to parse and get ground truth.
# Runs the the model forward process on the words and compares results to ground truth. 
# Prints accuracy of test.
# Fails if a word in text is not in word_encoding.
def model_test(model, text, word_encoding, tag_encoding):
    correct = 0
    total = 0
    
    ground_truth = pos_tag(text)
    test_sentence, test_tags = unzip(ground_truth)
    
    assert all([x in word_encoding for x in test_sentence]), 'all words must occur within word_encoding'
    
    tag_scores = model_run_raw(model, test_sentence, word_encoding, tag_encoding)
    tag_results = tag_scores_to_results(tag_scores, tag_encoding)
        
    results = [x == y for x,y in zip(test_tags, tag_results)]
    correct += results.count(True)
    total += len(results)
    
    print('Model Result:', list(zip(test_sentence, tag_results)))
    print('Ground Truth:', ground_truth)
    print('Accuracy: %d/%d (%0.02f%%)' % (correct, total, correct/total*100))

    
# Given a model, training_data, word_encoding dictionary, tag_encoding dictionary, and an
# (optional) number of epochs, trains the model.
# Currently uses training data as validation data (which is a bad practice),
# but is fine for this toy problem. We just want to see the model improve as trained to
# know things are working. We want the users to validate the model as part of the pset.
def model_train(model, training_data, word_encoding, tag_encoding, epochs=300):
    loss_function = nn.NLLLoss()
    optimizer = optim.SGD(model.parameters(), lr=0.1)
    
    t_init = time.time()

    for epoch in range(epochs):  # again, normally you would NOT do 300 epochs, it is toy data
        t = time.time()
        print('EPOCH :: %d/%d' % (epoch+1,epochs))
        for sentence, tags in training_data:
            # Step 1. Remember that Pytorch accumulates gradients.
            # We need to clear them out before each instance
            model.zero_grad()

            # Step 2. Get our inputs ready for the network, that is, turn them into
            # Tensors of word indices.
            sentence_in = apply_encoding(sentence, word_encoding)
            targets = apply_encoding(tags, tag_encoding)

            # Step 3. Run our forward pass.
            tag_scores = model(sentence_in)

            # Step 4. Compute the loss, gradients, and update the parameters by
            # calling optimizer.step()
            loss = loss_function(tag_scores, targets)
            loss.backward()
            optimizer.step()
        
        print('Duration: %0.02f sec' % (time.time() - t))
        
        # we're using training data as validation data
        # bad practice but fine for this toy problem
        model_validate(model, training_data, word_encoding, tag_encoding)
        
        print()
    
    print('Completed Training in: %0.02f sec' % (time.time() - t_init))
            
# Given a list and an encoding dictionary, applies the encoding and packages the result as a tensor
def apply_encoding(lst, encoding):
    return torch.tensor([encoding[x] for x in lst], dtype=torch.long)

# Takes a model, parsed list of words, a word_encoding dictionary, and a tag_encoding dictionary.
# Runs the full forward process and returns tag_scores tensor. Each layer of the tensor represents
# a pdf of the possible classifications of the given word.
# "Raw" because it provides a "less-friendly" interface of model_run.
def model_run_raw(model, words, word_encoding, tag_encoding):
    # See what the scores are after training
    with torch.no_grad():
        inputs = apply_encoding(words, word_encoding)
        tag_scores = model(inputs)
        return tag_scores

# Takes tag_scores (output of model_run_raw/the model's forward process) and finds
# the argmax per word (i.e. most likely classification per word). Using tag_encodings,
# converts most-likely indices into corresponding tag string. Returns list of tag strings.
def tag_scores_to_results(tag_scores, tag_encoding):
    tag_encoding_rev = reverse_dict(tag_encoding)
    results = []
    
    for i in range(len(tag_scores)):
        idx = int(torch.argmax(tag_scores[i]))
        results.append(tag_encoding_rev[idx])
    
    return results

# Takes a model, a text string to processed, a word_encoding dictionary, and a tag_encoding dictionary.
# Runs the full forward process and returns a list tuples, each containing one split word and the
# models most-likely corresponding tag.
# Fails if a word in text is not in word_encoding.
def model_run(model, text, word_encoding, tag_encoding):
    words = split_words(text)
    assert all([x in word_encoding for x in words]), 'all words must occur within word_encoding'
    tag_scores = model_run_raw(model, words, word_encoding, tag_encoding)
    results = list(zip(words, tag_scores_to_results(tag_scores, tag_encoding)))
    return results

# Returns True if the text does not appear in the training corpus (i.e. original sentence)
# Returns False if the sentence appears in the training corpus (i.e. not original sentence)
def is_original_sentence(text, training_data):
    sentence = split_words(text)
    training_data_sentences = [sentences for sentences,tags in training_data]
    return sentence not in training_data_sentences

# returns a list of tagged sentences that contain the provided word
def get_sentences_with(word, training_data):
    return [sentence for sentence in training_data if word in sentence[0]]

# prints the sentences that contain the provided word
def print_sentences_with(word, training_data):
    results = get_sentences_with(word, training_data)
    for i,(words,tags) in enumerate(results):
        print('%d:' % i, ' '.join(words))
        print(list(zip(words,tags)))
        print()
    if len(results) == 0:
        print('Word not found in training_data!')

# dictionary of upenn_tags
# key: tag
# value: tuple of full tag name and examples of text classified with that tag
upenn_tags = {
    "$": ("dollar",
          "$ -$ --$ A$ C$ HK$ M$ NZ$ S$ U.S.$ US$"),
    "''": ("closing quotation mark",
           "' ''"),
    "(": ("opening parenthesis",
          "( [ {"),
    ")": ("closing parenthesis",
          ") ] }"),
    ",": ("comma",
          ","),
    "--": ("dash",
           "--"),
    ".": ("sentence terminator",
          ". ! ?"),
    ":":("colon or ellipsis",
         ": ; ..."),
    "CC": ("conjunction, coordinating",
           "& 'n and both but either et for less minus neither nor or plus so therefore times v. versus vs. whether yet"),
    "CD": ("numeral, cardinal",
           "mid-1890 nine-thirty forty-two one-tenth ten million 0.5 one forty-seven 1987 twenty '79 zero two 78-degrees eighty-four IX '60s .025 fifteen 271,124 dozen quintillion DM2,000 ..."),
    "DT": ("determiner",
           "all an another any both del each either every half la many much nary neither no some such that the them these this those"),
    "EX": ("existential there",
           "there"),
    "FW": ("foreign word",
           "gemeinschaft hund ich jeux habeas Haementeria Herr K'ang-si vous lutihaw alai je jour objets salutaris fille quibusdam pas trop Monte terram fiche oui corporis ..."),
    "IN": ("preposition or conjunction, subordinating",
           "astride among uppon whether out inside pro despite on by throughout below within for towards near behind atop around if like until below next into if beside ..."),
    "JJ": ("adjective or numeral, ordinal",
           "third ill-mannered pre-war regrettable oiled calamitous first separable ectoplasmic battery-powered participatory fourth still-to-be-named multilingual multi-disciplinary ..."),
    "JJR": ("adjective, comparative",
            "bleaker braver breezier briefer brighter brisker broader bumper busier calmer cheaper choosier cleaner clearer closer colder commoner costlier cozier creamier crunchier cuter ..."),
    "JJS": ("adjective, superlative",
            "calmest cheapest choicest classiest cleanest clearest closest commonest corniest costliest crassest creepiest crudest cutest darkest deadliest dearest deepest densest dinkiest ..."),
    "LS": ("list item marker",
           "A A. B B. C C. D E F First G H I J K One SP-44001 SP-44002 SP-44005 SP-44007 Second Third Three Two * a b c d first five four one six three two"),
    "MD": ("modal auxiliary",
           "can cannot could couldn't dare may might must need ought shall should shouldn't will would"),
    "NN": ("noun, common, singular or mass",
           "common-carrier cabbage knuckle-duster Casino afghan shed thermostat investment slide humour falloff slick wind hyena override subhumanity machinist ..."),
    "NNP": ("noun, proper, singular",
            "Motown Venneboerger Czestochwa Ranzer Conchita Trumplane Christos Oceanside Escobar Kreisler Sawyer Cougar Yvette Ervin ODI Darryl CTCA Shannon A.K.C. Meltex Liverpool ..."),
    "NNPS": ("noun, proper, plural",
             "Americans Americas Amharas Amityvilles Amusements Anarcho-Syndicalists Andalusians Andes Andruses Angels Animals Anthony Antilles Antiques Apache Apaches Apocrypha ..."),
    "NNS": ("noun, common, plural",
            "undergraduates scotches bric-a-brac products bodyguards facets coasts divestitures storehouses designs clubs fragrances averages subjectivists apprehensions muses factory-jobs ..."),
    "PDT": ("pre-determiner",
            "all both half many quite such sure this POS: genitive marker ' 's"),
    "PRP": ("pronoun, personal",
            "hers herself him himself hisself it itself me myself one oneself ours ourselves ownself self she thee theirs them themselves they thou thy us"),
    "PRP$": ("pronoun, possessive",
             "her his mine my our ours their thy your"),
    "RB": ("adverb",
           "occasionally unabatingly maddeningly adventurously professedly stirringly prominently technologically magisterially predominately swiftly fiscally pitilessly ..."),
    "RBR": ("adverb, comparative",
            "further gloomier grander graver greater grimmer harder harsher healthier heavier higher however larger later leaner lengthier less- perfectly lesser lonelier longer louder lower more ..."),
    "RBS": ("adverb, superlative",
            "best biggest bluntest earliest farthest first furthest hardest heartiest highest largest least less most nearest second tightest worst"),
    "RP": ("particle",
           "aboard about across along apart around aside at away back before behind by crop down ever fast for forth from go high i.e. in into just later low more off on open out over per pie raising start teeth that through under unto up up-pp upon whole with you"),
    "SYM": ("symbol",
            "% & ' '' ''. ) ). * + ,. < = > @ A[fj] U.S U.S.S.R * ** ***"),
    "TO": ("'to' as preposition or infinitive marker",
           "to"),
    "UH": ("interjection",
           "Goodbye Goody Gosh Wow Jeepers Jee-sus Hubba Hey Kee-reist Oops amen huh howdy uh dammit whammo shucks heck anyways whodunnit honey golly man baby diddle hush sonuvabitch ..."),
    "VB": ("verb, base form",
           "ask assemble assess assign assume atone attention avoid bake balkanize bank begin behold believe bend benefit bevel beware bless boil bomb boost brace break bring broil brush build ..."),
    "VBD": ("verb, past tense",
            "dipped pleaded swiped regummed soaked tidied convened halted registered cushioned exacted snubbed strode aimed adopted belied figgered speculated wore appreciated contemplated ..."),
    "VBG": ("verb, present participle or gerund",
            "telegraphing stirring focusing angering judging stalling lactating hankerin' alleging veering capping approaching traveling besieging encrypting interrupting erasing wincing ..."),
    "VBN": ("verb, past participle",
            "multihulled dilapidated aerosolized chaired languished panelized used experimented flourished imitated reunifed factored condensed sheared unsettled primed dubbed desired ..."),
    "VBP": ("verb, present tense, not 3rd person singular",
            "predominate wrap resort sue twist spill cure lengthen brush terminate appear tend stray glisten obtain comprise detest tease attract emphasize mold postpone sever return wag ..."),
    "VBZ": ("verb, present tense, 3rd person singular",
            "bases reconstructs marks mixes displeases seals carps weaves snatches slumps stretches authorizes smolders pictures emerges stockpiles seduces fizzes uses bolsters slaps speaks pleads ..."),
    "WDT": ("WH-determiner",
            "that what whatever which whichever"),
    "WP": ("WH-pronoun",
           "that what whatever whatsoever which who whom whosoever"),
    "WP$": ("WH-pronoun, possessive",
            "whose"),
    "WRB": ("Wh-adverb",
            "how however whence whenever where whereby whereever wherein whereof why"),
    "``": ("opening quotation mark",
           "` ``"),
}