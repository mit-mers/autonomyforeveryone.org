// Place references to all TPN XSD schema files here. These will be loaded
// into the VM used by libxml2 (via empscripten) to validate against.
schema_files = [
  {url: "schemas/temporal-networks.xsd", name: "temporal-networks.xsd", data: ""},
  {url: "schemas/tpn-defs.xsd", name: "tpn-defs.xsd", data: ""},
  {url: "schemas/tn.xsd", name: "tn.xsd", data: ""},
  {url: "schemas/tpn.xsd", name: "tpn.xsd", data: ""},
];



/* Function to validate a TPN. Calls libxml. */
function validate_tpns(tpns_xml) {
  console.log("Validating...");
  // Make sure that all schemas are actually loaded
  for(var i = 0; i < schema_files.length; i++) {
      if (schema_files[i].data == "") {
          console.error("Not all schemas loaded yet!");
          return false;
      }
  }

  // Set up a set of files to load into the libxml VM simulated by emscripten
  var files = [{name: 'tpn.xml', data: tpns_xml}];
  for(var i = 0; i < schema_files.length; i++) {
      files.push({
          name: schema_files[i].name,
          data: schema_files[i].data,
      });
  }

  // Prepare options to our modified libxml build
  var options = {
      files: files,
      xml_file: 'tpn.xml',
      schema_file: 'tpn.xsd',
  };

  // Call it!
  var result = validateXML(options);

  // Return a value
  return result;

}



/* Puts in AJAX requests to load all of the relevant
 XML schemas, so that validation can occur. */
function load_schema_files() {
  function generate_setter(i) {
      function setter(data) {
          schema_files[i].data = data;
      }
      return setter;
  }
  // Place an ajax request to laod each schema file
  for(var i = 0; i < schema_files.length; i++) {
      var file = schema_files[i];
      $.ajax({
          url: file.url,
          success: generate_setter(i),
          error: function(e) {error("Couldn't load a schema file");},
          dataType: 'text',
      });
  }
}


/* Defines some useful constants for SVG styling the TPN 

Note that this could also be put into a CSS file, but that breaks
SVG exporting. We want to make an SVG in the DOM that can be 
exported "as is", without relying on external CSS formatting.

*/
tpn_style = {
	'edge_color': '#333',
	'edge_width': '1.5',

	'labels': {
		'dispatch': {
			'background': '#ffc0c0',
			'border': '#9a4545',
			'color': '#9a4545',
			'border-width': '1',
			'radius': '5', 
			'padding': 3
		},
		'method-call': {
			'background': '#f0bcff',
			'border': '#8b4593',
			'color': '#8b4593',
			'border-width': '1',
			'radius': '5', 
			'padding': 3
		},
		'name': {
			'background': '#c8e7ff',
			'border': '#2b5575',
			'color': '#2b5575',
			'border-width': '1',
			'radius': '5', 
			'padding': 3
		},
		'duration': {
			'background': '#fff',
			'border': '#fff',
			'color': '#000',
			'border-width': '0',
			'radius': '5', 
			'padding': 3
		},
		'guard': {
			'background': '#ffeda7',
			'border': '#8c7105',
			'color': '#8c7105',
			'border-width': '1',
			'radius': '5', 
			'padding': 3
		},
		'state-constraint': {
			'background': '#beffb0',
			'border': '#306d1d',
			'color': '#306d1d',
			'border-width': '1',
			'radius': '5', 
			'padding': 3
		}

	}

};

function load_tpn(tpn) {

  // Create a new directed graph. Intetionally global variable.
  var g = new dagreD3.Digraph();
  g.graph({
    rankDir: "LR",
  });

  // Process a listing of state variables.
  var state_variables = {}
  var svs_top = tpn.children("state-variables");
  var svs = svs_top.children("state-variable")
  for (var i = 0; i < svs.length; i++) {
    var state_variable = $(svs[i]);
    var id = state_variable.children("id").text();
    var name = state_variable.children("name").text();
    // Todo: maybe get domain too.
    // Store it
    var sv = {
      id: id,
      name: name,
    }
    state_variables[id] = sv;
  }

  // Process a listing of decision variables
  var decision_variables = {}
  var dvs_top = tpn.children("decision-variables");
  var dvs = dvs_top.children("decision-variable")
  for (var i = 0; i < dvs.length; i++) {
    var decision_variable = $(dvs[i]);
    var id = decision_variable.children("id").text();
    var name = decision_variable.children("name").text();
    var at_event = decision_variable.children("at-event").text();
    var controllability = decision_variable.children("type").text();
    // Todo: maybe get domain too.
    // Store it
    var dv = {
      id: id,
      name: name,
      at_event: at_event,
      controllability: controllability,
    }
    decision_variables[id] = dv;
  }

  var info = {
    state_variables: state_variables,
    decision_variables: decision_variables,
  }

  var events_top = tpn.children("events");
  var events = events_top.children("event");
  for (var i = 0; i < events.length; i++) {
    var event = $(events[i]);
    var id = event.children("id").text();
    var name = event.children("name").text();
    var guard = event.children("guard");
    // Look up any choice variables for this event
    choices_at_event = get_choices_at_event(decision_variables, id);
    var value = {
      id: id,
      name: name,
      guard: guard,
      label: name,
      choices_at_event: choices_at_event,
      description: "<strong><h4 class=\"tooltip-title\">Event</h4></strong><strong>ID:</strong> " + id + "<br/><strong>Name:</strong> " + name + "<br/><strong>Guard:</strong> " + guard_to_string(guard, info) + "<br/><strong>Choices here:</strong> " + choices_at_event.map(function(dv) {return dv.id + " (" + dv.controllability + ")"}).join(", "),
      useDef: get_event_class_type(choices_at_event),
    }
    g.addNode(id, value);
  }


  var temporal_constraints_top = tpn.children("temporal-constraints");
  var temporal_constraints = temporal_constraints_top.children("temporal-constraint");
  for (var i = 0; i < temporal_constraints.length; i++) {
    var tc = $(temporal_constraints[i]);
    var id = tc.children("id").text();
    var name = tc.children("name").text();
    var fromevent = tc.children("from-event").text();
    var toevent = tc.children("to-event").text();
    var duration = parseDuration(tc.children("duration"));
    var guard = tc.children("guard");
    var value = {
      id: id,
      name: name,
      fromevent: fromevent,
      toevent: toevent,
      dispatch: dispatch,
      duration: duration,
      guard: guard,
      style: "stroke: " + tpn_style['edge_color'] + "; stroke-width: " + tpn_style['edge_width'] + "; fill: none;",
      label: "",
      description: "<strong><h4 class=\"tooltip-title\">Temporal Constraint</h4></strong><br/><strong>ID:</strong> " + id + "<br/><strong>Name:</strong> " + name + "<br/><strong>From:</strong> " + fromevent + "<br/><strong>To:</strong> " + toevent + "<br/><strong>Duration:</strong> " + duration.toString() + "</br/><strong>Guard:</strong>: " + guard_to_string(guard, info),
    }
    g.addEdge(id, fromevent, toevent, value);
  }

  var episodes_top = tpn.children("episodes");
  var episodes = episodes_top.children("episode");
  for (var i = 0; i < episodes.length; i++) {
    var episode = $(episodes[i]);
    var id = episode.children("id").text();
    var name = episode.children("name").text();
    var dispatch = episode.children("dispatch").text();
    var state_constraint = episode.children("state-constraint");
    var fromevent = episode.children("from-event").text();
    var toevent = episode.children("to-event").text();
    var duration = parseDuration(episode.children("duration"));
    var guard = episode.children("guard");
    var macro_tpn_id = episode.children("macro-tpn-id").text();
    var value = {
      id: id,
      name: name,
      fromevent: fromevent,
      toevent: toevent,
      dispatch: dispatch,
      duration: duration,
      guard: guard,
      state_constraint: state_constraint,
      macro_tpn_id: macro_tpn_id,
      label: "",
      style: "stroke: #333; stroke-width: 1.5; fill: none;",
      description: "<strong><h4 class=\"tooltip-title\">          Episode          </h4></strong><br/><strong>ID:</strong> " + id + "<br/><strong>Name:</strong> " + name + "<br/><strong>From:</strong> " + fromevent + "<br/><strong>To:</strong> " + toevent + "<br/><strong>Duration:</strong> " + duration.toString() + "</br/><strong>Dispatch:</strong> " + dispatch + "<br/><strong>Guard:</strong>: " + guard_to_string(guard, info) + "<br/><strong>State Constraint:</strong> " + state_constraint_to_string(state_constraint, info) + "<br/><strong>Macro TPN ID:</strong> " + macro_tpn_id,
    }
    g.addEdge(id, fromevent, toevent, value);
  }

  return {
    id: $(tpn.children("id")).text(),
    name: $(tpn.children("name")).text(),
    graph: g,
    graph_simplified: get_simplified_graph(g),
    info: info,
  };

}


/* Functions for TPN rendering */

function setup_tpn_renderer() {
  
  // Set up an SVG group so that we can translate the final graph.
  // TODO: fix here, multiple graphs. THis isn't using Dagre D3 properly.
  var svg = d3.select('svg');
  var svgGroup = svg.append('g');

  // Override drawNodes to set up the hover.
  
}


function draw_graph(g) {
  var renderer = new dagreD3.Renderer();

  // Use tipsy to render tooltips over the nodes
  var oldDrawNodes = renderer.drawNodes();
  renderer.drawNodes(function(g, svg) {
    var svgNodes = oldDrawNodes(g, svg);
    // Set the title on each of the nodes and use tipsy to display the tooltip on hover
    svgNodes.attr('title', function(d) {return g.node(d).description})
            .each(function(d) {$(this).tipsy({gravity: 'n', opacity: 0.9, html: true, delayIn: 200}); });
    return svgNodes;
  });

  // Also use tipsy to render tooltips on the edges
  var oldDrawEdgeLabels = renderer.drawEdgeLabels();
  renderer.drawEdgeLabels(function(g, svg) {
    var svgEdgeLabels = oldDrawEdgeLabels(g, svg);
    // Set the title on each and use tipsy
    svgEdgeLabels.attr('title', function(d) {return g.edge(d).description})
                .each(function(d) {$(this).tipsy({gravity: 'n', opacity: 0.9, html: true, delayIn: 200}); });
    return svgEdgeLabels;
  })


  var svgGroup = d3.select('svg g');


  // Custom transition function
  function transition(selection) {
    return selection.transition().duration(350);
  }
  renderer.transition(transition);
  // Run the renderer. This is what draws the final graph.
  layout = renderer.run(g, svgGroup);

  // Trigger MathJax to update any embedded math on the graph.
  MathJax.Hub.Queue(["Typeset",MathJax.Hub,"tpn-display"]);
}