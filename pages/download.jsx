import matter from 'gray-matter'
import Loader from 'react-loader-spinner'
import { useState } from 'react';
import Layout from '@components/Layout'
import MarkdownRender from '@components/MarkdownRender'

export default function Download({ siteTitle, description, markdownBody }) {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [institution, setInstitution] = useState("");
  const [waiting, setWaiting] = useState(false);

  /* URLs in the form of ["kirk-[platform]-[instructions]-[version].tar.gz", "https://signed-link.tar.gz"] */
  const [urls, setUrls] = useState([]);
  const [serverError, setServerError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    setWaiting(true);
    try {
      const res = await fetch(process.env.NEXT_PUBLIC_KIRK_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email_address: email,
          name,
          institution
        })
      });

      const body = await res.json();
      setUrls(body['urls']);
      setServerError(body['error']);
    } catch (e) {
      console.error(e);
    } finally {
      setWaiting(false);
    }
  }

  const renderForm = () => {
    return (
      <form onSubmit={handleSubmit}>
        <div className="mb-6">
          <label htmlFor="name" className="block mb-2 text-sm text-black">Name</label>
          <input type="text"
            name="name"
            id="name"
            value={name}
            onChange={e => setName(e.target.value)}
            required
            className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300"
          />
        </div>

        <div className="mb-6">
          <label htmlFor="institution" className="text-sm text-black">Institution</label>
          <input type="text"
            name="institution"
            id="institution"
            value={institution}
            onChange={e => setInstitution(e.target.value)}
            required
            className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300"
          />
        </div>

        <div className="mb-6">
          <label htmlFor="email" className="block mb-2 text-sm text-black">Email Address</label>
          <input type="email"
            name="email"
            id="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            required
            className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300" />
        </div>

        <div className="mb-6">
          <button type="submit"
            className="w-full px-3 py-4 text-white bg-indigo-500 rounded-md focus:bg-indigo-600 focus:outline-none">Submit</button>
        </div>
      </form>
    )
  }

  const renderWaiting = () => {
    return <Loader type="TailSpin" color="#9a3234" height={80} width={80} />
  }

  const renderUrls = () => {
    return <div className="grid justify-items-center">
      <p className="italic">Your download is ready! Pick the download for your system below.</p>
      {
        urls.map((url, index) =>
          <a key={`DOWNLOAD_${index}`} href={url[1]} download target="_blank">
            <button className="mt-2 bg-mersRed px-3 py-4 text-white bg-indigo-500 rounded-md focus:outline-none">{url[0]}</button>
          </a>
        )
      }
    </div>
  }

  const renderError = () => {
    return <div className="text-red-900 italic">{serverError}</div>
  }

  const haveURLs = urls.length > 0;

  return (
    <>
      <Layout pageTitle={`${siteTitle} | Download`} description={description}>
        <MarkdownRender className="prose text-black ml-auto mr-auto" source={markdownBody} />
        <div className="grid justify-items-center mt-8">
          {
            // when a user first arrives
            !waiting && !haveURLs && renderForm()
          }

          {
            // when a user submitted but we haven't received it yet
            waiting && renderWaiting()
          }

          {
            // when a user gets a good response
            !waiting && haveURLs && renderUrls()
          }

          {
            // when a user got an error
            !waiting && serverError !== "" && renderError()
          }
        </div>
      </Layout>
    </>
  )
}

export async function getStaticProps() {
  const content = await import('./download.md')
  const config = await import('../siteconfig.json')
  const data = matter(content.default)

  return {
    props: {
      siteTitle: config.title,
      description: config.description,
      markdownBody: data.content,
    },
  }
}
