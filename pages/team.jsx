import Layout from '@components/Layout'

const Team = ({ title, description, ...props }) => {
  return (
    <>
      <Layout pageTitle={`${title} | Team`} description={description}>
        This is our team!
      </Layout>
    </>
  )
}

export default Team

export async function getStaticProps() {
  const configData = await import(`../siteconfig.json`)

  return {
    props: {
      title: configData.default.title,
      description: configData.default.description,
    },
  }
}