export default function Footer() {
  const year = (new Date()).getFullYear();
  return (
    <footer className="p-6 bg-gray-900 text-white">
      <div className="flex flex-col sm:flex-row sm:justify-between">
        <div>
          (c) Model-based Embedded and Robotics Systems Group (MERS), MIT {year}
          <p>
            <a href="https://www.csail.mit.edu/research/model-based-embedded-and-robotics-systems-group" className="underline">MERS Homepage</a>
          </p>
          <p>
            <a href="https://gitlab.com/mit-mers/autonomyforeveryone.org" className="underline">Site source code</a>
          </p>
        </div>
        <div>
          Taught in collaboration with Airbus
          <p>
            Sponsor: <a href="mailto:sebastien.boria@airbus.com" className="underline">Sebastien Boria</a>
          </p>
        </div>
      </div>
    </footer>
  )
}
