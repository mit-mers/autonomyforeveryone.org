import itertools
import mdptoolbox
import numpy as np
from support import *
from testcases import *


reward_profile = {
    "movement": -1,
    "failure": -100,
    "success": 20
}

def makeBoard(size, state):
    board = np.zeros((size,size))
    agent1 = state[0]
    agent2 = state[1]
    rabbit = state[2]

    def placeObject(obj, pos, board):
        row = (size - 1) - (pos // size)
        column = pos % size
        board[row][column] = obj
    for obj, pos in ([1,agent1], [2,agent2], [3,rabbit]):
        placeObject(obj,pos, board)
    return board

def makeStateSpace(numAgents, numSpaces):
    possible_places = []
    for i in range(numAgents+1):
        agents_spaces = list(range(numSpaces))
        possible_places.append(agents_spaces)

    non_tagged = list(itertools.product(*possible_places))
    tagged = list(itertools.product(*possible_places[1:], ["tag"]))
    return [*non_tagged, *tagged]

def getActionSpace(numAgents):
    a = {'up', 'down', 'left', 'right', 'tag'}
    action_per_agent = [a for i in range(numAgents)]
    return np.array(list(itertools.product(*(action_per_agent))))

def getTransition(A, S, size, state_to_index):
    rabbit_actions = {'up', 'down', 'left', 'right'}
    t = np.zeros((len(A), len(S), len(S)))
    for a, actions in enumerate(A):
        both_tag = np.all(np.array(actions) == 'tag')
        either_tag = np.any(np.array(actions) == 'tag')
        for s, state in enumerate(S):
            all_same_pos = np.all(np.array(state) == state[0])
            has_tag = state[-1] == 'tag'
            for rabbit_action in rabbit_actions:
                new_actions = [*actions, rabbit_action]
                state_prime = tuple(returnPosition(new_actions[index], pos, size) for index, pos in enumerate(state))
                sp = state_to_index[state_prime]
                if has_tag:
                    t[a][s][sp] = 1
                    continue
                    
                if both_tag and all_same_pos:
                    state_prime = list(state_prime)
                    state_prime[-1] = "tag"
                    sp = [i for i in range(len(S)) if S[i] == tuple(state_prime)][0]
                    t[a][s][sp] = 1
                    continue
                
                t[a][s][sp] += 1/4

    return t

def getRewards(A, S, size, state_to_index):
    """
    A - the complete action space of the robots
    S - the complete state space of the game
    """
    reward_matrix = np.ones((len(S), len(A))) * reward_profile["movement"]
    for state in range(len(S)):
        for action in range(len(A)):
            if "tag" in A[action] and (A[action][0] != "tag" or A[action][1] != "tag"):
                reward_matrix[state][action] = reward_profile["failure"]
            elif (A[action][0] == "tag" and A[action][1] == "tag") and np.all(np.array(S[state]) == S[state][0]):
                reward_matrix[state][action] = reward_profile["success"]

            elif (A[action][0] == "tag" and A[action][1] == "tag") and not np.all(np.array(S[state]) == S[state][0]):
                reward_matrix[state][action] = reward_profile["failure"]

    return reward_matrix

def setRewards(new_reward_profile):
    default_rewards = {
        "movement": -1,
        "failure": -100,
        "success": 20
    }
    reward_profile = default_rewards.copy()
    for key in new_reward_profile:
        reward_profile[key] = new_reward_profile[key]
    return 'Rewards set'


def runBaGa(I, TH, A, prob_histories, Z, S, T, R, s_t, size, Q, findPolicies):
    utility = {}
    # state_start_index = (s_t[0] * size**4)  + (s_t[1]*size**2)
    # valid_states = [state_start_index+x for x in range(size+1)]
    valid_states_indexes = list(i for i in range(len(S)) if S[i][:2] == s_t[:-1])
    joint_histories = [t for i in I for t in TH[i]]
    for a, actions in enumerate(A):
        actions = tuple(actions)
        utility[actions] = {}
        for theta in joint_histories:
            theta = tuple(theta)
            utility[actions].setdefault(theta, 0)
            for s in valid_states_indexes:
                # print(s_t, S[s])
                rabbit_with_agents = (S[s][-1] == s_t[0]) and (S[s][-1] == s_t[1])
                see_rabbit = theta[-1] != 0

                # 1. Robot doesn't see rabbit: belief of states where rabbit is in my position is 0
                # 2. Robot doesn't see rabbit: belief of states where rabbit is in not my position is 1/(size**2 -1)
                # 3. Robot does see rabbit: belief of state where rabbit is in my position is 1 all others 0
                belief = 1 if rabbit_with_agents else 0
                belief = 1 if see_rabbit else belief

                if see_rabbit:
                    if actions == ("tag", "tag"):
                        # print(s_t)
                        pass
                    if S[s][-1] == S[s][0] and S[s][-1] == S[s][1]:
                        belief = belief
                    else:
                        belief = 0

                else:
                    belief = 1 / (size**2 - 1)

                if actions == ("tag", "tag"):
                    pass
                    # print(theta, S[s], belief, rabbit_with_agents, Q.Q[s][a], A[a])
                utility[actions][theta] += belief * Q[s][a]

    sigma = findPolicies(prob_histories, utility)
    
    return [sigma, TH, prob_histories]


def run_algo(findPolicies, verbose=False):
    time = 20
    agents = 2
    size = 2

    A = getActionSpace(agents)
    action_to_index = {tuple(action): a for a, action in enumerate(A)}
    if verbose: print(len(A), 'action space')
    S = makeStateSpace(agents, size*size)
    if verbose: print(len(S), 'state space')
    state_to_index = {state: s for s, state in enumerate(S)}
    T = getTransition(A, S, size, state_to_index)
    if verbose: print(np.array(T).shape, 'transition space')
    R = getRewards(A, S, size, state_to_index)
    I = {1,2}


    Z = {0,1}
    seed = 0
    s_t = (1, 1, 0)
    # s_t = (0, 0, 0)
    prob_histories = dict()
    see = 0
    TH = {
        1: [[0]],
        2: [[0]],
    }
    for i in range(2):
        if s_t[i] == s_t[-1]:
            see += 1
            TH[i+1][0] = [1]
    if see == 0:
        prob_histories = {(0,): 1}
    if see == 1:
        prob_histories = {(0,): .5, (1,): .5}
    if see == 2:
        prob_histories = {(1,): 1}
    r = 0
    if verbose: print("##### STARTING BOARD #####")
    if verbose: displayBoard(size, s_t)
    if verbose: print("##### GAME BEGIN #####")
    qmdp = None
    for t in range(time):
        if verbose:
            print(f"On timestep: {t}")

        theta = tuple([1])
        del qmdp
        qmdp = mdptoolbox.mdp.QLearning(T, R, 0.9)
        qmdp.run()
        Q = qmdp.Q
        data = [runBaGa(I, TH, A, prob_histories, Z, S, T, R, s_t, size, Q, findPolicies),
                runBaGa(I, TH, A, prob_histories, Z, S, T, R, s_t, size, Q, findPolicies)]

        sigma = tuple([data[i][0][i] for i in range(len(data))])
        # Check if gamestate is over
        if np.all(np.array(sigma) == 'tag') and np.all(np.array(s_t) == s_t[0]):
            if verbose:
                print("##### GAME WON #####")
                displayBoard(size, s_t)
            return r + 20, t
        sigma_with_rabbit = tuple([*sigma, rabbitAction(s_t, size)])
        s_t_next = tuple(returnPosition(sigma_with_rabbit[index], pos, size) for index, pos in enumerate(s_t))
        action_index = [i for i in range(len(A)) if tuple(A[i]) == sigma][0]
        s_index = state_to_index[s_t]
        r += R[s_index, action_index]
        s_t = s_t_next

        observation = [1 if s_t[i-1] == s_t[-1] else 0 for i in I]
        length = len(TH[1][0])
        TH[1][0].append(observation[0])
        TH[2][0].append(observation[1])

        for j in [i for i in prob_histories if len(i) == length]:
            j1 = (*j, 1)
            j0 = (*j, 0)
            prob_histories[j1] = prob_histories[j] / 2
            prob_histories[j0] = prob_histories[j] / 2
            del prob_histories[j]
    # If fails to find solution
    return -300, t


def runGame(findPolicies, verbose=False):
    total_time = 0
    reward = 0
    for i in range(7):
        print(f'Run {i} of algorithm')
        r, t = run_algo(findPolicies, verbose)
        total_time += t
        reward += r

    print("Average time: ", total_time / 7)
    print("Average reward: ", reward / 7)
    return total_time / 7, reward / 7

