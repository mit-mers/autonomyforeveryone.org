import matplotlib.pyplot as plt
import matplotlib.animation as animation

import seaborn as sns

def convert_state_to_plottable(state_array):
    x = []
    y = []
    for state in state_array:
        x.append(state.x)
        y.append(state.y)
    return x, y

def visualize_trajectory(state_array_1, state_array_2):
    x1, y1 = convert_state_to_plottable(state_array_1)
    x2, y2 = convert_state_to_plottable(state_array_2)
    plt.plot(x1, y1)
    plt.plot(x2, y2)

global anim    
def animate_trajectory(state_array_1, state_array_2):
    fig, ax = plt.subplots()
    x1, y1 = convert_state_to_plottable(state_array_1)
    x2, y2 = convert_state_to_plottable(state_array_2)

    line1, = ax.plot(x1, y1, color = "r")
    line2, = ax.plot(x2, y2, color = "g")

    ax.set_xlim(-5, 5)
    ax.set_ylim(-5, 5)

    def animate(num, x1, y1, x2, y2, line1, line2):
        line1.set_data((x1[:num], y1[:num]))
        line2.set_data((x2[:num], y2[:num]))
        return [line1, line2]

    global anim
    anim = animation.FuncAnimation(fig, animate, len(x1), fargs=[x1, y1, x2, y2, line1, line2],
                      interval=5, blit=True)
    plt.show()
