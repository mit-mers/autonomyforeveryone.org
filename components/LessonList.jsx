import _ from 'lodash';
import Link from 'next/link'
import MarkdownRender from '@components/MarkdownRender'

/**
 * Render a card showing information about the lesson
 * @param {lesson} lesson
 */
function LessonCard({ lesson: { frontmatter, markdownBody, slug } }) {
  const markdownSections = markdownBody.split("##")
  const introParagraphs = markdownSections[0] || "";

  const thumbnail = _.get(frontmatter, 'thumbnail', null);
  const ytVideoID = _.get(frontmatter, 'yt_video_id', null);

  return (
    <article className="w-100 max-w-screen-xl">
      <div className="bg-white text-black rounded overflow-hidden shadow-lg p-4 w-100">
        <div className="flex justify-between">
          <Link className="inline-block" href={{ pathname: `/lesson/${slug}` }}>
            <a>
              <h1 className="text-2xl">
                {frontmatter?.title}
              </h1>
            </a>
          </Link>
        </div>
        <div className="flex sm:flex-col md:flex-row">
            <div className="mr-2">
            <img src={thumbnail} style={{ maxWidth: "560px", height: "auto" }} alt="Lesson thumbnail" />
          </div>
        </div>
        <div className="mt-4 mb-4" >
          <p className="italic text-sm text-indent-2">By {frontmatter.author}. It takes approximately {frontmatter.timetofinish} to complete this lesson.</p>
          <MarkdownRender className="prose-lg" source={introParagraphs} />
        </div>
        <Link href={{ pathname: `/lesson/${slug}` }}>
          <a className="block m-auto w-48 border-2 rounded cursor-pointer text-center text-xl font-bold">
            Continue reading
          </a>
        </Link>
      </div>
    </article>
  )
}

export default function LessonList({ lessons }) {
  if (lessons === 'undefined') {
    return null
  }

  return (
    <div className="flex flex-row justify-center">
      {!lessons && <div>No lessons!</div>}
      <div className="grid grid-cols-1 sm:w-screen lg:w-3/4">
        {lessons &&
          lessons.filter(lesson => !lesson.frontmatter.draft).
            map((lesson) => {
            return (
              <div key={lesson.slug} className="my-4 w-100">
                <LessonCard lesson={lesson} />
              </div>
            )
          })}
      </div>
    </div>
  )
}
