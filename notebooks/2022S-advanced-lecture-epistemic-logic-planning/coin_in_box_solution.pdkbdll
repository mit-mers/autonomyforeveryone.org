(define (domain coin-in-box)
  (:agents a b c)
  (:types )
  (:constants )
  (:predicates
    (coin-tails)
    (locked)
    (seen-coin ?ag - agent)
    {AK}(looking ?ag - agent)
    {AK}(has-key ?ag - agent)
    {AK}(is-self ?ag1 ?ag2 - agent)
  )

  (:action unlock
    :derive-condition (looking $agent$)
    :parameters (?ag - agent)
    :precondition (and (has-key ?ag))
    :effect (and
      (!locked)
    )
  )

  (:action distract
    :derive-condition always
    :parameters (?ag1 ?ag2 - agent)
    :precondition (and (not (is-self ?ag1 ?ag2)) (looking ?ag2))
    :effect (and
      (!looking ?ag2)
    )
  )

  (:action signal
    :derive-condition always
    :parameters (?ag1 ?ag2 - agent)
    :precondition (and (not (is-self ?ag1 ?ag2)) (not (looking ?ag2)))
    :effect (and
      (looking ?ag2)
    )
  )

  (:action announce-heads
    :derive-condition always
    :parameters (?ag1 ?ag2 - agent)
    :precondition (and (not (is-self ?ag1 ?ag2)) (seen-coin ?ag1))
    :effect (and
      (when
        (not(seen-coin ?ag2))
        [?ag2](!coin-tails)
      )
    )
  )

  (:action announce-tails
    :derive-condition always
    :parameters (?ag1 ?ag2 - agent)
    :precondition (and (not (is-self ?ag1 ?ag2)) (seen-coin ?ag1))
    :effect (and
      (when
        (not(seen-coin ?ag2))
        [?ag2](coin-tails)
      )
    )
  )
  
  (:action peek
    :derive-condition always
    :parameters (?ag1 - agent)
    :precondition (and [?ag1](!locked) (looking ?ag1))
    :effect (and
      (forall ?ag2 - agent (and
        (when (and (!locked) (coin-tails) (looking ?ag2)) [?ag2](coin-tails))
        (when (and (!locked) (!coin-tails) (looking ?ag2)) [?ag2](!coin-tails))
        (when (and (!locked) (looking ?ag2)) (seen-coin ?ag2))
      ))
    )
  )
)

(define (problem coin1)
  (:domain coin-in-box)
  (:projection )
  (:depth 1)
  (:task valid_generation)
  (:init-type complete)
  (:init
    (coin-tails)
    (looking a)
    (looking b)
    (looking c)
    (has-key a)
    (locked)
    [a](locked)
    [b](locked)
    [c](locked)
    (is-self a a)
    (is-self b b)
    (is-self c c)
  )
  (:goal (and
    [a](coin-tails)
    [b](!coin-tails)
    [c](locked)
  ))
)