module.exports = {
  // global Tailwind options!
  theme: {
    // colors: {
    //   transparent: 'transparent',
    //   blue: '#00a',
    //   black: '#000',
    //   red: '#9a3335',
    //   white: '#fff',
    //   gray: {
    //     100: '#f7fafc',
    //     500: '#9e9e9e',
    //     900: '#1a202c',
    //   }
    // },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
    height: {
      'yt-embed': '315px'
    },
    minHeight: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      'full': '100%',
    },
    fontFamily: {
      display: ['Gilroy', 'sans-serif'],
      body: ['Graphik', 'sans-serif'],
    },
    borderWidth: {
      default: '1px',
      '0': '0',
      '2': '2px',
      '4': '4px',
    },
    extend: {
      colors: {
        cyan: '#9cdbff',
        mersRed: '#9a3234'
      },
      spacing: {
        '96': '24rem',
        '128': '32rem',
      }
    }
  },
  plugins: [
    require("@tailwindcss/typography"),
  ],
  // remove unused Tailwind CSS automatically
  purge: [
    './components/**/*.jsx',
    './pages/**/*.jsx'
  ],
  future: {
    removeDeprecatedGapUtilities: true,
  },
}
