import ReactMarkdown from 'react-markdown'
import matter from 'gray-matter'
import Layout from '@components/Layout'

export default function Researchers({ siteTitle, description, markdownBody }) {
  return (
    <div>
      <Layout pageTitle={`Research Outreach | ${siteTitle}`}
        description={description}>
        <ReactMarkdown className="prose lg:prose-xl text-gray-900" source={markdownBody} />
      </Layout>
    </div>
  )
}

export async function getStaticProps() {
  const content = await import('./researchers.md')
  const config = await import(`../siteconfig.json`)
  const data = matter(content.default)

  return {
    props: {
      siteTitle: config.title,
      description: config.description,
      markdownBody: data.content,
    },
  }
}
