# About Kirk

## v0.0.4

This is a very light version being released simply to check that pipelines
work. It is limited to simply checking for consistency of plans and dispatching
them using simulated time. To test that it is working, unpack the tarball (eg. `tar zxvf kirk-[version].tar.gz`), ensure `kirk` is in your `$PATH`, and run the following command from the root of the unpacked directory:

    kirk execute examples/morning-lecture/script.rmpl morning-lecture::main

### Download

In accordance with MIT policy, we need to collect some information in order to let you download `kirk`. Don't worry, we'll NEVER share your information with third parties. This is strictly for MERS record-keeping purposes. Also, if you've already given us your information, no worries. Just enter the same email address and you can re-download Kirk.
