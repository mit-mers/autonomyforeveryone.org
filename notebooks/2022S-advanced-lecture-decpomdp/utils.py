import copy
import abc
import itertools
import numpy as np

# ========== POMDP Formulation ==========

class POMDP:
  @property
  @abc.abstractmethod
  def state_space(self):
    raise NotImplementedError("Override me")
    
  @property
  @abc.abstractmethod
  def action_space(self):
    raise NotImplementedError("Override me")

  def state_is_terminal(self, state):
    return False

  @property
  @abc.abstractmethod
  def observation_space(self):
    raise NotImplementedError("Override me")
  
  @property
  @abc.abstractmethod
  def init_belief(self):
    raise NotImplementedError("Override me")

  @abc.abstractmethod
  def get_reward(self, action):
    raise NotImplementedError("Override me")

# ========== Dec-POMDP Formulation ==========
    
class decPOMDP:

  @property
  @abc.abstractmethod
  def pomdps(self):
    raise NotImplementedError("Override me")

  @property
  @abc.abstractmethod
  def jointActions(self):
    raise NotImplementedError("Override me")

  @property
  @abc.abstractmethod
  def jointObservations(self):
    raise NotImplementedError("Override me")

  @property
  @abc.abstractmethod
  def initJointBelief(self):
    raise NotImplementedError("Override me")

  @property
  def temporal_discount_factor(self):
        return 1.

  @property
  def horizon(self):
        return float("inf")

  @abc.abstractmethod
  def get_transition_distribution(self, jointAction):
      raise NotImplementedError("Override me")

  @abc.abstractmethod
  def get_observation_distribution(self, jointAction):
      raise NotImplementedError("Override me")

# ========== Single Agent Tiger Problem Formulation (POMDP) ==========
    
class TigerPOMDP(POMDP):
    def __init__(self, initBelief):
      super().__init__()
      self.initialBelief = initBelief
      
    def init_belief(self):
      return self.initialBelief
    
    def state_space(self):
      return {'TigerLeft', 'TigerRight'}
    
    def action_space(self):
      return {'Listen', 'OpenLeft', 'OpenRight'}

    def observation_space(self):
      return {'Rawr Left', 'Rawr Right'}
    
    def get_reward(self, action):
      if action == 'Listen':
        return np.matrix([-1.0, -1.0])
      if action == 'OpenLeft':
        return np.matrix([10, -100])
      if action == 'OpenRight':
        return np.matrix([-100, 10])

# ========== General Utility Functions ==========

def expand_pomdp(pomdp, policy):
  '''
  Returns a list of every possible policy with an extra horizon for POMDP
  '''
  if policy is None:
    children = []
    for action in pomdp.action_space():
      child = {action : [{obs : None} for obs in pomdp.observation_space()]}
      children.append(child)
    return children
  children = []
  policy_action = list(policy.keys())[0]
  observation_branches = list(policy.values())[0]
  one_branch = observation_branches[0]
  next_sub_policy = list(one_branch.values())[0]
  finishing_policies = expand_pomdp(pomdp, next_sub_policy)
  finishing_combinations = [p for p in itertools.product(finishing_policies, repeat=len(observation_branches))]
  for combination in finishing_combinations:
    new_policy = copy.deepcopy(policy)
    for sub_policy in range(len(observation_branches)):
      related_obervation = list(new_policy[policy_action][sub_policy].keys())[0]
      new_policy[policy_action][sub_policy][related_obervation] = combination[sub_policy]
    children.append(new_policy)
  return children


def expand(decpomdp, policy, h):
  '''
  Returns a list of every possible policy with an extra horizon for DecPOMDP
  '''
  new_individual_policies = []
  for agent in policy.keys():
    new_individual_policies.append([(agent, x) for x in expand_pomdp(agent, policy[agent])])
  combination = list(itertools.product(*new_individual_policies))
  new_policies = []
  for agents in combination:
    new_policy = {}
    for agent in agents:
      new_policy[agent[0]] = agent[1]
    new_policies.append((new_policy, h + 1))
  return new_policies


def get_all_branches_one_agent(policy):
  '''
  Returns every branch for a given policy tree of one agent
  '''
  if policy is None:
    return [[]]
  policy_action = list(policy.keys())[0]
  observation_branches = list(policy.values())[0]
  paths = []
  for obs_idx in range(len(observation_branches)):
    observation = list(observation_branches[obs_idx].keys())[0]
    paths_from_this_branch = get_all_branches_one_agent(observation_branches[obs_idx][observation])
    for path in paths_from_this_branch:
      paths.append([policy_action, observation] + path)
  return paths


def get_all_branches(policy):
  '''
  Returns every join branch for a given multi-agent policy tree
  '''
  agent_branch_pairs = []
  for agent in policy[0].keys():
    individual_branches = get_all_branches_one_agent(policy[0][agent])
    agent_branch_pairs.append([])
    for branch in individual_branches:
      agent_branch_pairs[-1].append([agent, branch])
  all_branches = list(itertools.product(*agent_branch_pairs))
  return all_branches


def branch_with_final_action_removed(branch):
  '''
  Returns the POMDP Branch with the final actions removed, if the branch has any actions at all
  '''
  if len(branch[0][1]) == 0:
    return branch
  new_branch = copy.deepcopy(branch)
  new_branch[0][1] = new_branch[0][1][:-2] 
  new_branch[1][1] = new_branch[1][1][:-2]   
  return new_branch


def consolidate_beliefs(belief):
  '''
  Returns one combined belief state from each individual agent's belief
  '''
  belief = [belief[0]*belief[2], belief[0]*belief[3]]
  return [x/sum(belief) for x in belief]

# ========== Dec-Tiger Specific Utility Functions ==========

def heuristic(state, joint_action):
  '''
  Precomputed heuristic for the Dec-Tiger problem
  '''
  if joint_action == ('Listen', 'Listen') : return -2
  if joint_action == ('Listen', 'OpenRight') : return -101 if state == 1 else 9
  if joint_action == ('Listen', 'OpenLeft') : return 9 if state == 1 else -101
  if joint_action == ('OpenRight', 'Listen') : return -101 if state == 1 else 9
  if joint_action == ('OpenRight', 'OpenRight') : return -50 if state == 1 else 20
  if joint_action == ('OpenRight', 'OpenLeft') : return -100
  if joint_action == ('OpenLeft', 'Listen') : return 9 if state == 1 else -101
  if joint_action == ('OpenLeft', 'OpenRight') : return -100
  if joint_action == ('OpenLeft', 'OpenLeft') : return 20 if state == 1 else -50

def observation_probability(decpomdp, joint_observation, joint_action, state):
  '''
  Returns probability of a joint observation given a joint action and previous state
  '''
  agent_1_obervation_probs, agent_2_obervation_probs = decpomdp.get_observation_distribution(joint_action)
  p_observation_given_s_and_action = 1000
  if joint_observation[0] == 'Rawr Left' and joint_observation[1] == 'Rawr Left': p_observation_given_s_and_action = agent_1_obervation_probs[0, state] * agent_2_obervation_probs[0, state]
  if joint_observation[0] == 'Rawr Left' and joint_observation[1] == 'Rawr Right':  p_observation_given_s_and_action = agent_1_obervation_probs[0, state] * agent_2_obervation_probs[1, state]
  if joint_observation[0] == 'Rawr Right' and joint_observation[1] == 'Rawr Left':  p_observation_given_s_and_action = agent_1_obervation_probs[1, state] * agent_2_obervation_probs[0, state]
  if joint_observation[0] == 'Rawr Right' and joint_observation[1] == 'Rawr Right':  p_observation_given_s_and_action = agent_1_obervation_probs[1, state] * agent_2_obervation_probs[1, state]
  return p_observation_given_s_and_action