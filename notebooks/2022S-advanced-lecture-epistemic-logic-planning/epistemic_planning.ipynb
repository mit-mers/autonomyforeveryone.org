{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c192342d",
   "metadata": {},
   "source": [
    "# Epistemic Planning as Classical Planning\n",
    "\n",
    "## Due Wednesday, April 25, 11:59 pm\n",
    "\n",
    "In this problem set we'll implement a subset of the RP-MEP (restricted perspectival multi-agent epistemic planning) algorithm discussed in class and demonstrate its usage in a few different domains that have been selected to highlight interesting aspects of modelling with epistemic logic. Recall that RP-MEP works by first converting from epistemic logic to propositional logic, including adding ancillary effects to actions to ensure consistent beliefs, then using a classical propositional planner."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "8262e27b",
   "metadata": {},
   "outputs": [],
   "source": [
    "from itertools import product\n",
    "from typing import List\n",
    "\n",
    "from epistemic_planning import (\n",
    "    Predicate,\n",
    "    Proposition,\n",
    "    Action,\n",
    "    convert_pdkbdll_to_pddl,\n",
    "    eplan,\n",
    "    test_ak_predicate_to_propositions,\n",
    "    test_belief_predicate_to_propositions,\n",
    "    test_negation_removal,\n",
    "    test_coin_in_box,\n",
    "    test_ok,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b779f87",
   "metadata": {},
   "source": [
    "## Intro to PDKBDLL syntax\n",
    "\n",
    "Epistemic planning tasks for RP-MEP are written in PDKBDLL (proper doxastic knowledge base domain description language) files. The PDKBDLL format is very similar to the PDDL format that you're familiar with from problem set 3. The main difference in PDKBDLL is that we have to list a set of agents in the domain and for each predicate, it is either \"always known\" (marked by `{AK}`) or can be believed by different agents (`[a](p)` means agent `a` believes `p`). AK predicates behave like predicates in PDDL do; we only reason about whether they hold, not whether a specific agent believes them. Additionally, we have to specify the depth of nested beliefs. A depth of 1 means agents can believe predicates (e.g. `[a](p)`). A depth of 2 allows beliefs about other agent's beliefs (e.g. `[b][a](p)`); increased depth increases the nesting of beliefs about beliefs. Some additional functionality is supported which you won't need for these problems; you can read the [RP-MEP][rp-mep1] [papers][rp-mep2] to learn more about those features and for the most detailed description of the algorithm.\n",
    "\n",
    "Below is a simple PDKBDLL file, with extensive comments, which demonstrates the syntax. In this domain, there's an agent `a` which can move in a corridor between two locations, `loc1` and `loc2`. Agent `b` is at location `loc2` and doesn't move, so this isn't represented explicitly with a predicate. If at `loc1`, `a` is able to learn a `secret`. If at `loc2`, `a` can share that secret with `b`. This problem is adapated from the corridor problem [here][corridor-domain] and [here][corridor-problem].\n",
    "\n",
    "```\n",
    "(define (domain corridor-demo) ; name the domain\n",
    "  (:agents a b) ; specify the agents; an agent type is created automatically, and these are instances of it\n",
    "  (:types loc) ; list any new types required for your problem\n",
    "  (:constants loc1 loc2 - loc) ; constant parameters in the domain\n",
    "  (:predicates ; list all predicates, including any parameters\n",
    "    (secret)\n",
    "    {AK}(at ?l - loc)\n",
    "    {AK}(succ ?l1 ?l2 - loc)\n",
    "  )\n",
    "\n",
    "  (:action right\n",
    "    :derive-condition   always ; don't modify\n",
    "    :parameters         (?l1 ?l2 - loc)\n",
    "    :precondition       (and (succ ?l1 ?l2) (at ?l1))\n",
    "    :effect             (and (!at ?l1) (at ?l2))\n",
    "  )\n",
    "  \n",
    "  (:action left\n",
    "    :derive-condition   always\n",
    "    :parameters         (?l1 ?l2 - loc)\n",
    "    :precondition       (and (succ ?l1 ?l2) (at ?l2))\n",
    "    :effect             (and (at ?l1) (!at ?l2))\n",
    "  )\n",
    "  \n",
    "  (:action check-secret\n",
    "    :derive-condition   always\n",
    "    :precondition       (and (at loc1))\n",
    "    :effect             (and [a](secret))\n",
    "  )\n",
    "  \n",
    "  (:action tell-secret\n",
    "    :derive-condition   always\n",
    "    :precondition       (and (at loc2) [a](secret))\n",
    "    :effect             (and [b](secret))\n",
    "  )\n",
    ")\n",
    "\n",
    "(define (problem corridor1) ; name the problem\n",
    "  (:domain corridor-demo) ; specify the domain (named above)\n",
    "  (:projection ) ; don't modify\n",
    "  (:depth 1) ; depth of nested beliefs\n",
    "  (:task valid_generation) ; don't modify (generate a valid plan to achieve the goal)\n",
    "  (:init-type complete) ; don't modify\n",
    "  (:init ; list predicates that hold in the initial state\n",
    "    (at loc1)\n",
    "    (succ loc1 loc2)\n",
    "  )\n",
    "  (:goal ; list goal predicates\n",
    "    [b](secret)\n",
    "  ) \n",
    ")\n",
    "```\n",
    "\n",
    "PDKBDLL also supports\n",
    "* conditional effects using `(when)`; this is like an if statement that allows for an effect to only be applied when the given condition holds\n",
    "* quantifying over all instances of a type using `(forall ?x - type)`\n",
    "\n",
    "Here's a simple example to show how both of these can be used.\n",
    "\n",
    "```\n",
    "(:predicates\n",
    "  (friendly ?ag - agent)\n",
    "  {AK}(friends ?ag1 ?ag2 - agent)\n",
    ")\n",
    "\n",
    "(:action make_friends\n",
    "  :derive-condition always\n",
    "  :parameters (?ag1 - agent)\n",
    "  :precondition (and)\n",
    "  :effect (and\n",
    "    (forall ?ag2 - agent\n",
    "      (when\n",
    "        [?ag1](friendly ?ag2) ; condition(s)\n",
    "        (friends ?ag1 ?ag2) ; effect\n",
    "      )\n",
    "    )\n",
    "  )\n",
    ")\n",
    "```\n",
    "\n",
    "Additional notes:\n",
    "* a PDKBDLL file includes both the domain and the problem, so you'll only need one file, unlike for PDDL\n",
    "* use `![a](p)` for `a` not believing `p`\n",
    "* don't forget the parentheses for beliefs; `[a](p)` is valid but `[a] p` is not\n",
    "* however, don't put parentheses around the belief operator itself. E.g. `(:init [a](trustworthy b))` is valid but `(:init ([a](trustworthy b)))` is not.\n",
    "* Always use `(and)` in your preconditions. So an empty precondition is `:precondition (and)` and a precondition with a single predicate is, e.g., `:precondition (and (at loc1))`\n",
    "* conditional effects don't work with `(and)` in the effect. Instead of `(when (p) (and (q) (w))` you have to break it into one conditional effect with just `q` and one with just w). Having an `(and)` in the condition is allowed, though; e.g. `(when (and (p) (q)) (w))`.\n",
    "* If you use a parameter named `?ag` for an action, make sure not to use `forall ?ag - agent` in the effect (use a new name like `forall ?ag2 - agent`).\n",
    "\n",
    "You can find several more example PDKBDLL files [here][examples] if you're interested (note that `{include:dom-agents3.pdkbddl}` means to insert the given file verbatim at this point in the source file; we won't use that syntax, but some of the examples in the link do).\n",
    "\n",
    "[rp-mep1]: https://www.aaai.org/ocs/index.php/AAAI/AAAI15/paper/viewFile/9974/9761\n",
    "[rp-mep2]: https://arxiv.org/pdf/2110.02480.pdf\n",
    "[corridor-domain]: https://github.com/QuMuLab/pdkb-planning/blob/master/examples/planning/corridor/dom-agents3.pdkbddl\n",
    "[corridor-problem]: https://github.com/QuMuLab/pdkb-planning/blob/master/examples/planning/corridor/prob-depth1.pdkbddl\n",
    "[examples]: https://github.com/QuMuLab/pdkb-planning/tree/master/examples/planning"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4005c427",
   "metadata": {},
   "source": [
    "## Problem 1 - Converting epistemic logic to propositional logic\n",
    "\n",
    "We'll break the RP-MEP algorithm down into a few interesting parts\n",
    "* converting AK predicates to propositions\n",
    "* converting belief predicates to propositions\n",
    "* adding ancillary effects to actions\n",
    "\n",
    "We don't require you to implement other aspects such as PDKBDLL file parsing and outputting a valid PDDL file, and you are only required to implement some of the ancillary effects discussed in lecture.\n",
    "\n",
    "We've created classes to represent predicates and propositions. A `Proposition` just has a name. A `Predicate` has a name and a set of arguments / parameters, each with a specified, finite domain. You can access these with the `arg_domains` attribute which is a mapping from argument names to a tuple of strings representing the possible values. Below are a couple of examples using these classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "018a641c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "secret\n"
     ]
    }
   ],
   "source": [
    "secret = Predicate(\"secret\")\n",
    "print(secret.name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "fcb9fc30",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'l1': ('l1', 'l2', 'l3', 'l4'), 'l2': ('l1', 'l2', 'l3', 'l4')}\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "succ(l1, l2)\n",
       "  l1 in (l1, l2, l3, l4)\n",
       "  l2 in (l1, l2, l3, l4)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "locations = (\"l1\", \"l2\", \"l3\", \"l4\")\n",
    "successor = Predicate(\"succ\", l1=locations, l2=locations)\n",
    "print(successor.arg_domains)\n",
    "successor"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00375226",
   "metadata": {},
   "source": [
    "### Converting AK predicates (5 pts)\n",
    "\n",
    "First we'll convert from an AK predicate to a set of propositions. We don't need to know about the agents or the nesting of belief yet. Implement the function `ak_predicate_to_propositions` to take a predicate as input and output a list of propositions that represent the possible valuations of that predicate with each possible set of inputs. E.g. for a predicate `p x y` where `x` can be `0` or `1` and `y` can be `red` or `blue`, you would need one proposition each for `p 0 red`, `p 0 blue`, `p 1 red`, and `p 1 blue` as well as a proposition for the negation of each (RP-MEP uses separate propositions for, e.g., `p 0 red` and `!p 0 red`, with ancillary effects to ensure that only one of these can hold).\n",
    "\n",
    "To allow for proper grading of your method, use the following naming conventions:\n",
    "\n",
    "| Predicate      | Proposition Name  |\n",
    "| -------------- | ----------------- |\n",
    "| `p arg1 arg2`  | `p_arg1_arg2`     |\n",
    "| `!p arg1 arg2` | `not_p_arg1_arg2` |\n",
    "\n",
    "Make sure that you can handle \"predicates\" with no parameters as well (e.g. `Predicate(\"secret\")`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "168a8609",
   "metadata": {},
   "outputs": [],
   "source": [
    "# you may find the product function from itertools useful\n",
    "# (we've already imported it for you)\n",
    "\n",
    "def ak_predicate_to_propositions(pred: Predicate) -> List[Proposition]:\n",
    "    \"\"\"\n",
    "    :param pred: an \"always-known\" (AK) predicate to convert to propositions\n",
    "    :returns: a list with one proposition for each possible set of inputs to\n",
    "      `pred` and one proposition for the negation of each, named as described\n",
    "      in the table above.\n",
    "    \"\"\"\n",
    "    #### BEGIN SOLUTION\n",
    "    if not pred.arg_domains:\n",
    "        return [Proposition(pred.name), Proposition(f\"not_{pred.name}\")]\n",
    "    props = []\n",
    "    for args in product(*pred.arg_domains.values()):\n",
    "        props.append(Proposition(f\"{pred.name}_{'_'.join(args)}\"))\n",
    "        props.append(Proposition(f\"not_{pred.name}_{'_'.join(args)}\"))\n",
    "    return props\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a4f052a0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"alert alert-success\">\n",
       "        <strong>Tests passed!!</strong>\n",
       "        </div>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "test_ak_predicate_to_propositions(ak_predicate_to_propositions)\n",
    "test_ok()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "603e8b4a",
   "metadata": {},
   "source": [
    "### Converting belief predicates (15 pts)\n",
    "\n",
    "Next you'll implement the function `belief_predicate_to_propositions` which does a similar conversion for predicates which may be believed. In order to generate a finite set of propositions, we'll need to know the maximum nesting of belief that's allowed as well as the set of possible agents. Then we can generate one proposition for each combination of agents, up to the maximum nesting, either believing or not believing each proposition possible from the predicate. With any amount of belief nesting, we also need to include the propositions for the predicate without any belief modality. E.g. if we have one agent `a` and a maximum belief nesting of 1 for a predicate `p x` with `x` either `0` or `1`, we'd need one proposition for each of\n",
    "* `p 0`, `p 1`, `!p 0`, `!p 1`\n",
    "* `[a](p 0)`, `[a](p 1)`, `[a](!p 0)`, `[a](!p 1)` and the negated beliefs `![a]` for each.\n",
    "\n",
    "Note that with a maximum belief nesting of 2, we need to account for both doubly-nested beliefs like `[a][a]` as well as singly-nested beliefs.\n",
    "\n",
    "To allow for proper grading of your method, use the following naming conventions:\n",
    "\n",
    "| Predicate             | Proposition Name         |\n",
    "| --------------------- | ------------------------ |\n",
    "| `[a](p arg1 arg2)`    | `Ba_p_arg1_arg2`         |\n",
    "| `![a](p arg1 arg2`    | `not_Ba_p_arg1_arg2`     |\n",
    "| `[a](! p arg1 arg2)`  | `Ba_not_p_arg1_arg2`     |\n",
    "| `![a](! p arg1 arg2)` | `not_Ba_not_p_arg1_arg2` |\n",
    "| `[b][a](p arg1 arg2)` | `Bb_Ba_p_arg1_arg2`      |\n",
    "\n",
    "You may want to make use of your `ak_predicate_to_propositions` function from above, though this isn't necessary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "c3792597",
   "metadata": {},
   "outputs": [],
   "source": [
    "def belief_predicate_to_propositions(\n",
    "    pred: Predicate, agents: List[str], max_belief_nesting: int\n",
    ") -> List[Proposition]:\n",
    "    \"\"\"\n",
    "    :param pred: a predicate to convert to propositions\n",
    "    :param agents: a list of the agents' names\n",
    "    :param max_belief_nesting: maximum number of times a belief operator can be\n",
    "      nested (e.g. 2 to allow `[a][b](p)`)\n",
    "    :returns: one proposition for each possible belief or negated belief of each\n",
    "      agent or nested set of agents for each proposition from the possible\n",
    "      inputs to `pred`\n",
    "    \"\"\"\n",
    "    # you can assume there's at least one agent and at least 1 level of belief\n",
    "    assert agents\n",
    "    assert max_belief_nesting >= 1\n",
    "\n",
    "    ### BEGIN SOLUTION\n",
    "    prefixes = []\n",
    "    for agent in agents:\n",
    "        prefixes.append(f\"B{agent}\")\n",
    "        prefixes.append(f\"not_B{agent}\")\n",
    "\n",
    "    props = ak_predicate_to_propositions(pred)\n",
    "    for belief_nesting in range(1, max_belief_nesting + 1):\n",
    "        for prefix in product(prefixes, repeat=belief_nesting):\n",
    "            prefix = \"_\".join(prefix)\n",
    "            sub_props = ak_predicate_to_propositions(pred)\n",
    "            props.extend(\n",
    "                Proposition(f\"{prefix}_{prop.name}\") for prop in sub_props\n",
    "            )\n",
    "    return props\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "502e24a4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"alert alert-success\">\n",
       "        <strong>Tests passed!!</strong>\n",
       "        </div>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "test_belief_predicate_to_propositions(belief_predicate_to_propositions)\n",
    "test_ok()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72de006c",
   "metadata": {},
   "source": [
    "### Ancillary effects (5 pts)\n",
    "\n",
    "To fully implement RP-MEP, we would also need to do a similar conversion on the actions so that they use the new propositions (which have no explicit belief modality) instead of the old predicates (which could have belief). This requires creating many additional actions which won't have parameters (e.g. an action `move` with parameters `l1` and `l2` which are either `loc1` or `loc2` would be converted to four actions: `move_l1_l1`, `move_l1_l2`, `move_l2_l1`, and `move_l2_l2`). You aren't required to implement this part, but we provide a function `convert_pdkbdll_to_pddl` which uses the [implementation][github] of RP-MEP from the authors. We'll use this later for planning, but if you're interested to see how the demo PDKBDLL file from the introduction would look when fully converted, you can run the cell below then check the files `domain.pddl` and `problem.pddl`.\n",
    "* Note that `convert_pdkbdll_to_pddl` will overwrite any existing PDDL files with the same name, so if you want to save them, be sure to pass in different domain and problem file names instead of using the default ones.\n",
    "* The belief propositions are named slightly differently than what we've had you do in your implementation. They use `Ba_secret`, `Ba_not_secret`, `Pa_not_secret`, `Pa_secret` where `Pa_secret` means \"`a` thinks `secret` is possible\", i.e. `not_Ba_not_secret` in our naming scheme. `Pa_not_secret` is thus the same as our `not_Ba_secret`.\n",
    "\n",
    "Recall from lecture that the ancillary effects are additional effects for the actions which are required to keep our beliefs consistent. E.g. if `[a](secret)` is an effect of an action, we must also have `![a](!secret)` as an effect since it's not consistent to believe that a proposition is true and false. We've provided an `Action` class for you which is shown below. It just stores the set of preconditions, positive effects (propositions added after the action), and negative effects (propositions removed after the action) as well as the action name. For simplicity, we're working in the space where the actions have already been partially converted so they use the new propositions instead of the old predicates and have no parameters. However, they don't yet necessarily maintain consistent beliefs.\n",
    "\n",
    "The only ancillary effect which you're required to implement here is negation removal. This ensures that if some proposition $\\phi$ holds after an action is taken, $\\lnot \\phi$ doesn't also hold. E.g. your function should convert\n",
    "\n",
    "```\n",
    "(:action move_l1_l2\n",
    "  :precondition (and (at_l1) (succ_l1_l2))\n",
    "  :effect (and\n",
    "    ; note that both of these are positive effects\n",
    "    (at_l2)\n",
    "    (not_at_l1)\n",
    "  )\n",
    ")\n",
    "```\n",
    "\n",
    "to\n",
    "\n",
    "```\n",
    "(:action move_l1_l2\n",
    "  :precondition (and at_l1 succ_l1_l2)\n",
    "  :effect (and\n",
    "    (at_l2)\n",
    "    (not_at_l1)\n",
    "    ; now we've added negative effects which delete the negations of\n",
    "    ; the positive effects\n",
    "    (not (not_at_l2))\n",
    "    (not (at_l1))\n",
    "  )\n",
    ")\n",
    "```\n",
    "\n",
    "[github]: https://github.com/QuMuLab/pdkb-planning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "73c8ff79",
   "metadata": {},
   "outputs": [],
   "source": [
    "convert_pdkbdll_to_pddl(\"demo-corridor.pdkbdll\")\n",
    "# check domain.pddl and problem.pddl;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "078d3007",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "move_l1_l2\n",
       "  Precond: [at_l1, succ_l1_l2]\n",
       "  Eff+: [at_l2, not_at_l1]\n",
       "  Eff-: []"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "move_l1_l2 = Action(\n",
    "    \"move_l1_l2\",\n",
    "    [\"at_l1\", \"succ_l1_l2\"],\n",
    "    [\"at_l2\", \"not_at_l1\"],\n",
    "    [],\n",
    ")\n",
    "move_l1_l2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "139aba70",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "move_l1_l2\n",
       "  Precond: [at_l1, succ_l1_l2]\n",
       "  Eff+: [at_l2, not_at_l1]\n",
       "  Eff-: [not_at_l2, at_l1]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# your function should convert move_l1_l2 to\n",
    "move_l1_l2_negation_removal = Action(\n",
    "    \"move_l1_l2\",\n",
    "    [\"at_l1\", \"succ_l1_l2\"],\n",
    "    [\"at_l2\", \"not_at_l1\"],\n",
    "    [\"not_at_l2\", \"at_l1\"],\n",
    ")\n",
    "move_l1_l2_negation_removal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "8de9900c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# you may find it useful to define a `negate` helper function, though\n",
    "# this isn't required\n",
    "\n",
    "def negate(prop: Proposition) -> Proposition:\n",
    "    \"\"\"Returns the negation of `prop`.\"\"\"\n",
    "    ### BEGIN SOLUTION\n",
    "    if prop.name.startswith(\"not_\"):\n",
    "        return Proposition(prop.name.replace(\"not_\", \"\", 1))\n",
    "    return Proposition(f\"not_{prop.name}\")\n",
    "    ### END SOLUTION\n",
    "\n",
    "def negation_removal(action: Action) -> None:\n",
    "    \"\"\"\n",
    "    Modifies `action` in-place so that if any proposition is added as an\n",
    "    effect, the negation of that proposition is also deleted.\n",
    "    \"\"\"\n",
    "    ### BEGIN SOLUTION\n",
    "    for prop in action.pos_effects:\n",
    "        action.neg_effects.append(negate(prop))\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "66d70685",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"alert alert-success\">\n",
       "        <strong>Tests passed!!</strong>\n",
       "        </div>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "test_negation_removal(negation_removal)\n",
    "test_ok()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a482f733",
   "metadata": {},
   "source": [
    "## Problem 2 - Modelling with epistemic logic (25 pts)\n",
    "\n",
    "Now that we've gained a better understanding of how RP-MEP works by implementing part of it, we'll create a PDKBDLL file of our own. We're going to model a coin in box scenario similar to the one from the lecture. We've provided a starter file for you, `coin_in_box.pdkbdll`. The predicates and action names are already there; don't add any new actions or predicates as this may interfere with the auto-grading. You just need to add the preconditions and effects of the actions and the initial and goal formulas.\n",
    "\n",
    "You may want to reference the mini tutorial on PDKBDLL syntax from the start of the pset, especially the notes which highlight some of the easy mistakes to make. One additional note on PDKBDLL syntax: you should still use `(not (p))` for `p` not being in the list of propositions holding (e.g. `:precondition (and (not (p)))` for an action that's only valid if `p` isn't in your list of propositions). Using `!p` means you require `(not_p)` to be in the list of propositions holding. When talking about belief, you'll always want to use `!` (as in `[a](!p)`, `![a](!p)`, etc.). To illustrate this point, consider the action `unlock` which we've implemented for you:\n",
    "\n",
    "```\n",
    "(:action unlock\n",
    "  :derive-condition (looking $agent$)\n",
    "  :parameters (?ag - agent)\n",
    "  :precondition (and (has-key ?ag))\n",
    "  :effect (and\n",
    "    (!locked)\n",
    "  )\n",
    ")\n",
    "```\n",
    "\n",
    "If you convert this to PDDL and look at `domain.pddl`, you'll see that the action `unlock_a` has effects like\n",
    "\n",
    "```\n",
    "; #25276: origin\n",
    "(not_locked)\n",
    "; #71017: <==negation-removal== 25276 (pos)\n",
    "(not (locked))\n",
    "```\n",
    "\n",
    "(in addition to the many other belief ones). After `unlock` happens, another action could use `(not (locked))` or `(!locked)` - which translates to `(not_locked)` - as a precondition since `locked` no longer holds and `not_locked` now does. However, if you look at `problem.pddl`, you'll see that `(:init)` contains\n",
    "\n",
    "```\n",
    "(is-self_a_a)\n",
    "(is-self_b_b)\n",
    "(is-self_c_c)\n",
    "```\n",
    "\n",
    "but not terms like `not_is-self_a_b`. So you could use `(not (is-self a b))` as a precondition, but `(!is-self a b)` wouldn't work as expected because that doesn't hold (unless you explicitly add terms like `(!is-self a b)` to `(:init)`). If you don't want to think about the distinction between `not` and `!`, it's probably safe to use `not` whenever belief isn't involved and `!` otherwise.\n",
    "\n",
    "\n",
    "#### Initial formulas\n",
    "\n",
    "* All agents should be looking\n",
    "* the coin is tails\n",
    "* `a` has the key\n",
    "* the box is locked\n",
    "* all agents believe the box is locked\n",
    "\n",
    "Add these in the `(:init)` section\n",
    "\n",
    "#### Goal formulas\n",
    "\n",
    "* `a` belives the coin is tails\n",
    "* `b` believes the coin is heads (i.e. not tails)\n",
    "* `c` believes the box is locked\n",
    "\n",
    "Add these in the `(:goal)` section\n",
    "\n",
    "#### Actions\n",
    "\n",
    "**unlock**\n",
    "* Unlock the box so agents can peek in to see if the coin is heads or tails\n",
    "* This action is already complete. Note that we use `:derive-condition (looking $agent$)` so that only agents who are looking observe the effect of the action, that the box becomes unlocked. The ancillary effects will update the beliefs of the looking agents so they'll believe the box is unlocked (without us needing to explicitly update any beliefs). This uses the conditioned mutual awareness ancillary effect discussed in lecture, but you don't need to use any derived conditions for the actions you complete.\n",
    "\n",
    "**distract**\n",
    "* one agent distracts another so the latter agent is no longer looking at the box\n",
    "* Preconditions\n",
    "  * the two agents are not the same (use `is-self`)\n",
    "  * the second agent is looking\n",
    "* Effects\n",
    "  * the second agent is not looking\n",
    "\n",
    "**signal**\n",
    "* one agent signals another so the latter looks at the box\n",
    "* Preconditions\n",
    "  * the two agents are not the same (use `is-self`)\n",
    "  * the second agent is not looking\n",
    "* Effects\n",
    "  * the second agent is looking\n",
    "\n",
    "**announce-heads**\n",
    "* one agent tells another that the coin is heads\n",
    "* Preconditions\n",
    "  * the two agents are not the same (use `is-self`)\n",
    "  * the first agent has seen the coin (use `seen-coin`)\n",
    "* Effects\n",
    "  * if (i.e. `when`) the second agent has not seen the coin, the second agent believes the coin is heads (i.e. not tails)\n",
    "\n",
    "**announce-tails**\n",
    "* one agent tells another that the coin is tails\n",
    "* Preconditions\n",
    "  * the two agents are not the same (use `is-self`)\n",
    "  * the first agent has seen the coin (use `seen-coin`)  \n",
    "* Effects\n",
    "  * if (i.e. `when`) the second agent has not seen the coin, the second agent believes the coin is tails\n",
    "\n",
    "**peek**\n",
    "* Peek into the box to see the coin\n",
    "* Preconditions\n",
    "  * the agent believes the box is not locked\n",
    "  * the agent is looking\n",
    "* Effects\n",
    "  * all agents (use `forall`) have the following effects\n",
    "    * if the box is not locked and the agent is looking and the coin is heads, the agent believes the coin is heads\n",
    "    * if the box is not locked and the agent is looking and the coin is tails, the agent believes the coin is tails\n",
    "    * if the box is not locked and the agent is looking, the agent has seen the coin\n",
    "\n",
    "\n",
    "#### Testing\n",
    "\n",
    "Once you've finished the PDKBDLL file, you can use the `convert_pdkbdll_to_pddl` function mentioned above to make the PDDL files. We encourage you to look at these and see if they make sense and if you understand how the predicates and actions were transformed. Then you can use `eplan` to generate the plan (if you use different names for the PDDL files than the default ones, make sure to give the same names to both of these functions).\n",
    "\n",
    "#### Comments\n",
    "\n",
    "This domain highlights some interesting aspects of modelling with epistemic logic: \n",
    "* Preconditions can depend on your belief while effects can depend on the actual state of the world. E.g. the `peek` action requires an agent to believe that the box is unlocked but the effects only occur if it is actually unlocked. There could possibly be a different set of effects that happen when the box is still locked but you try to peek.\n",
    "* We can't model disjunctive beliefs like (`a` believes the coin is heads OR `a` believes the coin is tails). However, we can use a separate predicate like our `seen-coin` which is true iff an agent believes the coin to be heads or tails because it observed the coin itself. In a more complicated domain, though, it could be difficult to ensure that a predicate always holds when some disjunction of beliefs does (the ancillary effects won't account for this, so you would need to manually ensure the predicate is updated when it should be).\n",
    "* Multiple agents acting: RP-MEP doesn't allow for any parallelism between different agents acting, but we can generate a centrally-coordinated plan with actions that should be taken by different agents (e.g. there are multiple solutions to this coin-in-box domain depending on which agents distract which other ones and in which order)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "195d6826",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['']"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "convert_pdkbdll_to_pddl(\"coin_in_box.pdkbdll\")\n",
    "plan = eplan()\n",
    "plan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "02b82354",
   "metadata": {},
   "outputs": [
    {
     "ename": "RuntimeError",
     "evalue": "Expected A to announce-heads to B as final action.",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mRuntimeError\u001b[0m                              Traceback (most recent call last)",
      "\u001b[0;32m/tmp/ipykernel_760/2618377796.py\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mtest_coin_in_box\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"coin_in_box.pdkbdll\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mprint_plan\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mFalse\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      2\u001b[0m \u001b[0mtest_ok\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      3\u001b[0m \u001b[0;31m# if you think the plan is valid but the auto-grader doesn't, feel free to\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      4\u001b[0m \u001b[0;31m# post on piazza. I've tried to make the autograder accept any valid plan\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      5\u001b[0m \u001b[0;31m# for this task that doesn't include unnecessary actions, but I could have\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/tmp/eplan/epistemic_planning/epistemic_planning.py\u001b[0m in \u001b[0;36mtest_coin_in_box\u001b[0;34m(fname, print_plan)\u001b[0m\n\u001b[1;32m    458\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    459\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mplan\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpop\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m!=\u001b[0m \u001b[0;34m\"ANNOUNCE-HEADS(A, B)\"\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 460\u001b[0;31m         \u001b[0;32mraise\u001b[0m \u001b[0mRuntimeError\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"Expected A to announce-heads to B as final action.\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    461\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mplan\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpop\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m!=\u001b[0m \u001b[0;34m\"PEEK(A)\"\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    462\u001b[0m         \u001b[0;32mraise\u001b[0m \u001b[0mRuntimeError\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"Expected A to peek as penultimate action.\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mRuntimeError\u001b[0m: Expected A to announce-heads to B as final action."
     ]
    }
   ],
   "source": [
    "test_coin_in_box(\"coin_in_box.pdkbdll\", print_plan=False)\n",
    "test_ok()\n",
    "# if you think the plan is valid but the auto-grader doesn't, feel free to\n",
    "# post on piazza. I've tried to make the autograder accept any valid plan\n",
    "# for this task that doesn't include unnecessary actions, but I could have\n",
    "# missed something"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48e11d9f",
   "metadata": {},
   "source": [
    "Don't forget to validate the notebook and submit it!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "e7370f93d1d0cde622a1f8e1c04877d8463912d04d973331ad4851f04de6915a"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
