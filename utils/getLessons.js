import matter from 'gray-matter'

/**
 * Sort lessons based on the frontmatter date, newest to oldest
 * @param {object} a
 * @param {object} b
 */
const dateSorter = (a, b) => {
  const [yyyy1, mm1, dd1] = a.frontmatter.date.split("-")
  const [yyyy2, mm2, dd2] = b.frontmatter.date.split("-")
  const d1 = new Date(+yyyy1, +mm1, +dd1)
  const d2 = new Date(+yyyy2, +mm2, +dd2)
  return d1 > d2 ? -1 : 0
}

/**
 * Parse lesson markdown files. Returns them newest to oldest
 * @param {NodeRequire.context} context
 * @returns {Object[]}
 */
export default function getLessons(context) {
  const keys = context.keys()
  const values = keys.map(context)

  const data = keys.map((key, index) => {
    let slug = key.replace(/^.*[\\\/]/, '').slice(0, -3)
    const value = values[index]
    const doc = matter(value.default)
    return {
      frontmatter: doc.data,
      markdownBody: doc.content,
      slug,
    }
  })

  data.sort(dateSorter)
  return data
}
