from scipy.stats import bernoulli
import time, sys
import random
from IPython.display import clear_output
import matplotlib.pyplot as plt
import numpy as np

class BernoulliDist():
    """
    A class representing a bernouli distribution
    """
    def __init__(self, p):
        self.p = p
        self.rv = bernoulli(p)
        
    def sample(self):
        return self.rv.rvs()

    
class BanditProblem:
    """
    A class representing a bandit problem
    """
    def __init__(self, reward_dists=[]):
        """
        Initialize a bandit problem class
        
        Parameters:
        reward_dists: a list of BernouliDist representing reward distributions.
        """
        self.reward_dists = reward_dists
    
    def get_reward(self, action_index):
        """
        Return the reward for the action
        """
        return self.reward_dists[action_index].sample()

    def get_num_actions(self):
        """
        Return the number of available actions
        """
        return len(self.reward_dists)

    
def make_bernouli_bandit_problem(p_values):
    """
    Generate a BanditProblem with Bernouli reward distributions
    """
    dists = [BernoulliDist(x) for x in p_values]
    bp = BanditProblem(dists)
    return bp




# progress bar
# credit: https://www.mikulskibartosz.name/how-to-display-a-progress-bar-in-jupyter-notebook/
def update_progress(progress):
    bar_length = 20
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
    if progress < 0:
        progress = 0
    if progress >= 1:
        progress = 1

    block = int(round(bar_length * progress))

    clear_output(wait = True)
    text = "Progress: [{0}] {1:.1f}%".format( "#" * block + "-" * (bar_length - block), progress * 100)
    print(text)
    
def plot_etc(k, n, etc_func):
    assert(k >= 2)

    trials = 100 # monte carlo runs
    if n >= 100:
        trials = 50
    if n >= 250:
        trials = 30
    m_values = [i+1 for i in range(n//k)]
    
    best_arm_p_value = 0.9
    epsilon = 0.2
    p_values = [max(random.random()-epsilon, 0) for x in range(k)]
    temp = random.choice(p_values)
    p_values[p_values.index(temp)] = best_arm_p_value
    print("Ground truth p values (for your reference): ", p_values)
    time.sleep(2)
    
    reward_dists = [BernoulliDist(p) for p in p_values]
    bp = BanditProblem(reward_dists)

    rewards = []
    for i in range(len(m_values)):
        m = m_values[i]
        c_reward = 0
        for _ in range(trials):
            r, _ = etc_func(m, n, bp)
            c_reward += r
        rewards.append(c_reward / trials)
        update_progress(i / ( len(m_values) - 1))

    # plot the rewards
    plt.plot(m_values, rewards, linewidth=2.0)
    plt.title('Total Reward Obtained Over '+str(n)+' Samples with '+str(k)+' Zones')
    plt.xlabel('m')
    plt.ylabel('Reward')
    plt.grid(True)
    
def plot_ucb(k, n, ucb_func):
    trials = 50 # monte carlo runs
    if n > 100:
        trials = 20
    if n > 1000:
        trials = 5
    
    delta_values = list(np.linspace(0.01,0.99,99))

    best_arm_p_value = 0.9
    epsilon = 0.2
    p_values = [max(random.random()-epsilon, 0) for x in range(k)]
    temp = random.choice(p_values)
    p_values[p_values.index(temp)] = best_arm_p_value
    print("Ground truth p values (for your reference): ", p_values)
    time.sleep(2)
    
    reward_dists = [BernoulliDist(p) for p in p_values]
    bp = BanditProblem(reward_dists)

    rewards = []
    for i in range(99):
        delta = delta_values[i]
        c_reward = 0
        for trial in range(trials):
            r, a = ucb_func(delta, n, bp)
            c_reward += r
        rewards.append(c_reward / trials)
        update_progress(i / 99)

    # plot the rewards
    plt.plot(delta_values, rewards, linewidth=2.0)
    plt.title('Total Reward Obtained Over '+str(n)+' Samples with '+str(k)+' Zones')
    plt.xlabel('delta')
    plt.ylabel('Reward')
    plt.grid(True)
