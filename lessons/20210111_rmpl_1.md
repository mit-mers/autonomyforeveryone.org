---
title: Introduction to Model-based Programming
author: Marlyse Reeves
date: '2021-01-11'
timetofinish: 1 hour
thumbnail: '/images/mars_cartoon.png'
yt_video_id: 'vg3XpcgCkM8'
jupyter_url: '/jl/retro/notebooks/index.html?path=group-mtg1.ipynb'
gitlab_url: 'https://gitlab.com/mit-mers/jupyter/rmpl-tutorial-1'
---

An introduction to model-based programming using the Reactive Model-based Programming Language (RMPL), developed by the MERS group. RMPL is a language designed for planning and execution with a focus on expressive constraints. Capturing the essence of "model-based programming", RMPL gives users the ability program an agent by outlining how the agent is expected to behave during execution at a high-level, abstracting away low-level details.

In this lesson, you'll learn the basic constructs of RMPL, the temporal plan network (TPN) representation for expressing temporal plans, and an algorithm to check temporal consistency. You can start by watching the tutorial video or go straight to the hands-on exercises in Jupyter Notebook!
