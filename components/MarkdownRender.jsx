import Tex from '@matejmazur/react-katex'
import ReactMarkdown from 'react-markdown'
import rehypeKatex from 'rehype-katex'
import rehypeRaw from 'rehype-raw'
import remarkGfm from 'remark-gfm'
import remarkMath from 'remark-math'
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import QuestionBox from '@components/QuestionBox'

import 'katex/dist/katex.min.css'

export default function MarkdownRender({ source, className }) {
  const components = {
    code({ node, inline, className, children, ...props }) {
      const match = /language-(\w+)/.exec(className || '')
      return !inline && match ? (
        <SyntaxHighlighter
          children={String(children).replace(/\n$/, '')}
          language={match[1]}
          PreTag="div"
          {...props}
        />
      ) : (
        <code className={className} {...props}>
          {children}
        </code>
      )
    },
    inlineMath: ({ value }) => <Tex math={value} />,
    math: ({ value }) => <Tex block math={value} />,
    questionbox: ({ node }) => {
      // node might be some kind of remark thing
      return <QuestionBox>{node.children}</QuestionBox>
    },
    a: ({ href, children }) => {
      const newline = "&#13";
      return <a className="text-blue-500 underline" href={href} target="_blank">{children.join(newline)}</a>
    }
  }

  return (
    <ReactMarkdown className={className}
      remarkPlugins={[remarkGfm, remarkMath]}
      rehypePlugins={[rehypeKatex, rehypeRaw]}
      components={components}
      children={source} />
  )
}
