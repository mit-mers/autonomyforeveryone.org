import matter from 'gray-matter'
import Layout from '@components/Layout'

export default function About({ siteTitle, description }) {
  return (
    <>
      <Layout pageTitle={`${siteTitle} | About`} description={description}>
        <div className="bg-white text-black rounded overflow-hidden shadow-lg p-4 w-100">
          <p>We're a devoted group of autonomy researchers, engineers, and educators and we want you to learn about autonomy!</p>

          <div className="grid gap-12 grid-cols-1 md:grid-cols-2 lg:grid-cols-3">

            <BioCard name="Marlyse Reeves" bio="Marlyse is a PhD student in the EECS department at MIT. She received her B.S. in Aeronautics and Astronautics from MIT in 2017. Marlyse completed her Master's in Computer Science in Jan. 2020 with a focus on multi-agent online planning and execution. Her current research interests include robust execution, active learning and active sensing, and multi-agent autonomous observing systems. Marlyse loves doing STEM outreach and mentors an all-girls First Tech Challenge robotics team, the Winsor Wildbots. Outside of the lab, Marlyse's hobbies include playing soccer, dancing, and reading science fiction." picPath="./images/marlyse.jpeg" />

            <BioCard name="Cameron Pittman" bio="Cameron Pittman is an AeroAstro masters student in the Model-Based Embedded and Robotics Systems lab, where he studies distributed planning for planetary exploration. His research will enable teams of humans and robots to coordinate under uncertain communication conditions while exploring the Moon and Mars. Cameron is also a part-time human spaceflight software engineer and researcher at NASA Johnson Space Center. There, he leads a software team in developing decision support systems to augment the safety and efficiency of astronauts on spacewalks. His software is also used to analyze the health and safety of the upcoming Orion Multipurpose Crew Vehicle. Cameron holds a masters in teaching (2011) from Belmont University and a bachelors in physics (2009) from Vanderbilt University." picPath="./images/cameron_pittman.jpg" />

            <BioCard name="Delia Stephens" bio="Delia is a 5-year SB/SM student at MIT AeroAstro expecting to graduate in 2022. Her research focuses on robust planning under uncertainty, and she's particularly interested in designing systems that enable safe human-robot interaction.  After MIT, Delia will commission as a Cyberspace Operations Officer in the Air Force. Outside of the lab, she loves to ride (and crash) bicycles of all kinds, and is a member of the MIT Club Cycling Team and Air Force Cyling Team." picPath="./images/delia.png" />

          </div>

          <p>
            The <a href="/jl" target="_blank">Jupyter notebooks</a> are running entirely in your brower with <a href="https://jupyterlite.readthedocs.io/en/latest/" target="_blank">JupyterLite</a>. Thanks to the power of <a href="https://webassembly.org/" target="_blank">WebAssembly</a>, Python is becoming available on the web without the need for backend server execution.
          </p>

          <p>This website and the lessons on it are the result of a collaboration with Airbus.</p>

        </div>
      </Layout>
    </>
  )
}

function BioCard({ name, picPath, bio }) {
  return (
    <div>
      <div className="max-w-sm rounded overflow-hidden shadow-lg bg-white">
        <div className="w-full">
          <img className="w-full" src={picPath} alt={`${name} headshot`} />
        </div>
        <div className="px-4 py-4">
          <p className="text-2xl">{name}</p>
          <div className="w-full prose-sm">
            <p>{bio}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export async function getStaticProps() {
  const config = await import('../siteconfig.json')

  return {
    props: {
      siteTitle: config.title,
      description: config.description,
    },
  }
}
