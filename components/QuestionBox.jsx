import { useState } from "react"

function concatElements(children) {
  return children.reduce((prev, curr) => {
    if (curr.type === "element") {
      return <span>{prev}<curr.tagName>{concatElements(curr.children)}</curr.tagName></span>
    } else if (curr.type === "text") {
      return <span>{prev}{curr.value}</span>;
    }
  }, "");
}

/**
 * Render a box that asks a question. Make students click to see the answer
 * @param {*} children In the form of
 *    [{type, tagName, children: [{type, value}], properties, position}]
 */
export default function QuestionBox({ children }) {
  let question, answer;

  children.forEach(child => {
    if (child.type !== "element") {
      return;
    }

    if (child.tagName === "question") {
      question = concatElements(child.children);
    }

    if (child.tagName === "answer") {
      answer = concatElements(child.children);
    }
  });

  const [answerTextColor, setAnswerTextColor] = useState("text-black")
  const toggleVisibility = () => answerTextColor === "text-black" ? setAnswerTextColor("text-white") : setAnswerTextColor("text-black")

  return (
    <div className="flex justify-center border-solid border-2 border-black p-1">
      <div className="flex flex-col justify-center">
        <div className="mb-4">
          <strong>Question</strong>: {question}
        </div>

        <div className="cursor-pointer" onClick={toggleVisibility} title="Click to reveal the answer">
          <strong>Answer</strong>: (click to reveal)
          <div className={`p-1 bg-black ${answerTextColor}`}>{answer}</div>
        </div>
      </div>
    </div>
  )
}
